﻿using System.Collections.Generic;
using System.Security.Policy;
using System.Threading.Tasks;

namespace AwaitAsyncExamples
{
    /// <summary>
    ///     Quando un Task è awaited il controllo ritorna subito al chiamante del metodo e verrà riesumato quando l'operazione
    ///     si concluderà.
    ///     La scrittua "async" davanti ad un metod non significa "questo metodo verrà automaticamente schedulato su un thread
    ///     asincrono" ma significa l'opposto: questo metodo contiene del codice che riguarda l'attesa di operazioni asincrone
    ///     e quindi verrà riscritto dal compilatore in modo che possa essere riesumato al momento giusto. Quindi la keyword
    ///     "async" semplicemente identifica un metodo che ha una chiamata "await" al suo interno, niente altro. I metodi
    ///     "async" rimangono nel thread corrente il più possibile, analogamente alla coroutine.
    ///     Analogamente il comando "await" non significa "questo metodo blocca il thread corrente fino a che l'operazione
    ///     asincrona viene completata, perchè altrimenti significherebbe che ritorneremmo ad una situazione sincrona che è ciò
    ///     che vogliamo evitare. Invece significa l'opposto: "await un task significa esaminare il task: se è già comkpletato
    ///     proseguire avanti come nella programmazione sincrona, altrimenti prende tutto il resto del metodo come
    ///     "continuation" (vedi la programmazione CPS) e ritornare immediatamente al chiamante. Il task chiamerà
    ///     automaticamente la sua continuation (come lambda) una volta completato.
    ///     In un metodo "async" tutto è sincrono fino al metodo "await", è lì che le cose cominciano a diventare asincrone per
    ///     davvero. La keyword "await" prende in ingresso una operazione "awaitable".
    ///     La chiave di tutto è che la keyword "await" ferma il metodo corrente fino a che il task non è completato (quindi
    ///     effettivamente aspetta) MA il thread corrente non è bloccato quindi è effettivamente asincrono.
    /// </summary>
    internal class ArchiveDocuments
    {
        private void ArchiveDocuments(List<Url> urls)
        {
            for (int i = 0; i < urls.Count; ++i)
                Archive(Fetch(urls[i]));
        }

        /// <summary>
        /// </summary>
        /// <param name="urls"></param>
        private async void ArchiveDocuments(List<Url> urls)
        {
            Task archive = null;
            for (int i = 0; i < urls.Count; ++i)
            {
                var document = await FetchAsync(urls[i]);
                if (archive != null)
                    await archive;
                archive = ArchiveAsync(document);
            }
        }

        private long ArchiveDocuments(List<Url> urls)
        {
            long count = 0;
            for (int i = 0; i < urls.Count; ++i)
            {
                var document = Fetch(urls[i]);
                count += document.Length;
                Archive(document);
            }

            return count;
        }

        private async Task<long> ArchiveDocumentsAsync(List<Url> urls)
        {
            long count = 0;
            Task archive = null;
            for (int i = 0; i < urls.Count; ++i)
            {
                var document = await FetchAsync(urls[i]);
                count += document.Length;
                if (archive != null)
                    await archive;
                archive = ArchiveAsync(document);
            }

            return count;
        }
    }
}