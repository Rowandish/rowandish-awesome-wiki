﻿using System;

namespace ContinuousPassingProgrammingExamples
{
    public class TryCatch
    {
        /// <summary>
        ///     Questa volta proviamo ad implementare il Try Catch utilizzando la CPS.
        /// </summary>
        public TryCatch()
        {
            //void Q()
            //{
            //    try
            //    {
            //        B(A());
            //    }
            //    catch
            //    {
            //        C();
            //    }
            //    D();
            //}

            A(
                x => B( // A's normal continuation
                    x, // B's argument
                    () => D( // B's normal continuation
                        qNormal, // D's normal continuation
                        qError), // D's error continuation
                    () => C( // B's error continuation
                        () => D( // C's normal continuation
                            qNormal, // D's normal continuation
                            qError), // D's error continuation
                        qError)), // C's error continuation
                () => C( // A's error continuation
                    () => D( // C's normal continuation
                        qNormal, // D's normal continuation
                        qError), // D's error continuation
                    qError)); // C's error continuation
        }

        private void qError()
        {
            Console.WriteLine("Ho avuto un errore");
        }

        private void qNormal()
        {
            Console.WriteLine("Non ho avuto alcun errore");

        }

        /// <summary>
        ///     il try è un attimo più difficile: deve impostare una continuazione per il try block ma non il per catch block. il
        ///     try invoca il <paramref name="tryBody" />che ha due continuazioni: <paramref name="outerNormal" /> in caso che vada
        ///     tutto bene e <paramref name="outerError"/> che deve essere racchiuso nel metodo <paramref name="catchBody"/>
        /// </summary>
        private void Try(Action<Action, Action> tryBody,
            Action<Action, Action> catchBody,
            Action outerNormal,
            Action outerError)
        {
            tryBody(outerNormal, () => catchBody(outerNormal, outerError));
        }

        /// <summary>
        ///     Il throw in CPS non fa altro che chiamare la sua continuazione in caso di errore e non chiamare mai la sua normale
        ///     continuazione
        /// </summary>
        private void Throw(Action normal, Action error)
        {
            error();
        }

        /// <summary>
        ///     Il metodo A (se fosse con codice normale) fa un throw subito e poi ritorna 0 (anche se non ha senso assumiamolo
        ///     come esempio).
        ///     Dato che non può ritornare 0, lo 0 viene passata alla sua continuation normale.
        /// </summary>
        private void A(Action<int> normal, Action error)
        {
            //throw;
            //return 0; // unreachable, but let's ignore that

            Throw(() => normal(0), error);
        }

        /// <summary>
        ///     Ad ogni metodo dobbiamo passare due action: una per la continuazione normale del metodo e una seconda per la
        ///     continuazione in caso di errore. il metodo B ha un intero <paramref name="x"/> come parametro in ingresso che è quanto viene passato da A
        /// </summary>
        private void B(int x, Action normal, Action error)
        {
            Console.WriteLine("B");
        }

        private void C(Action normal, Action error)
        {
            Console.WriteLine("C");
        }

        private void D(Action normal, Action error)
        {
            Console.WriteLine("D");
        }
    }
}