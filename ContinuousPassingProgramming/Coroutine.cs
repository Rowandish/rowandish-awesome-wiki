﻿using System.Collections.Generic;

namespace ContinuousPassingProgrammingExamples
{
    /// <summary>
    ///     Una coroutine è un metodo che gira per un certo periodo di tempo, poi decide di "fermarsi" e permettere a altre
    ///     coroutine di poter lavorare. Quando qualche altra coroutine "ritorna il favore" la prima ricomincia esattamente da
    ///     dove l'altra aveva finito.
    /// </summary>
    public class Coroutine
    {
        public Coroutine()
        {
        }


    }
}