﻿using System;
using System.Diagnostics;

namespace ContinuousPassingProgrammingExamples
{
    internal class Program
    {
        
        private static void Main(string[] args)
        {
            new ConditionalIf();
            new TryCatch();
        }
    }
}