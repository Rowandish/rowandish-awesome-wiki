﻿using System;
using System.Diagnostics;

namespace ContinuousPassingProgrammingExamples
{
    internal class ConditionalIf
    {
        /// <summary>
        ///     Il Continuation Passing Style è uno stile di programmazione completamente diverso dal solito: il metodo classico è
        ///     dividere tutto in sottofunzioni che restituiscono valori i quali verranno combinati con delle variabili locali
        ///     salvate precedentemente per poter ottenere il risultato atteso. il CPS invece non prevede sottofunzioni e metodi
        ///     return, anzi l'ultima cosa che fa la funzione corrente è chiamare come lambda la prossima funzione e così via. Per
        ///     poter avere la certezza che le cose accadano in un determinato ordine, quando chiamo una funzione dovrò passare
        ///     anche la sua "continuazione" che è una funzione che tipicamente esegue tutto quello che viene dopo.
        ///     La programmazione CPS può essere usato per le ricorsioni ma anche per poter creare nuove primitive di controllo
        ///     flusso di un linguaggio implementando le vari parti come metodi.
        /// </summary>
        public ConditionalIf()
        {
            B(b => ShortIfWithCPS<int>(b, c => C(c), c => D(c), t => M(t, FinalContinuation)));
        }

        /// <summary>
        /// Continuazione finale del metodo
        /// </summary>
        private void FinalContinuation(string s)
        {
            Console.WriteLine($"Completato il giro, la stringa è: {s}");
            Console.ReadKey();
        }

        /// <summary>
        ///     Versione del comando ?: con CPS: se <paramref name="condition" /> è true allora chiamata il metodo consequence
        ///     passandogli in ingresso la sua continuation, che sarà quanto vorremo chiamare dopo aver effettuato consequence,
        ///     simmetricamente alternative. Il metodo ritorna void e usa la Action continuation per poter proseguire l'esecuzione
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="condition">Condizione booleana per poter distinguere la chiamata a consequence o alternative</param>
        /// <param name="consequence">Metodo da chiamare qualora <paramref name="condition" /> sia true</param>
        /// <param name="alternative">Metodo da chiamare qualora <paramref name="condition" /> sia false</param>
        /// <param name="continuation">
        ///     Metodo che dovrà essere chiamato alla fine dell'esecuzione del metodo, questo per avere un
        ///     metodo che ritorna void in stile CPS.
        /// </param>
        private static void ShortIfWithCPS<T>(bool condition, Action<Action<T>> consequence, Action<Action<T>> alternative,
            Action<T> continuation)
        {
            if (condition) consequence(continuation);
            else alternative(continuation);
        }


        /// <summary>
        ///     Metodo che ritorna un booleano. Visto che siamo in CPS invece di ritornare bool ritorno void ma chiamo una action
        ///     passata in ingresso nella quale fornirà il booleano calcolato
        /// </summary>
        private static void B(Action<bool> continuation)
        {
            Console.WriteLine("B");
            // ... calcolo l'output booleano di questo metodo
            var output = false;
            // Passo tale output a action
            continuation(output);
        }

        /// <summary>
        ///     Metodo che ritorna un intero. Visto che siamo in CPS invece di ritornare int ritorno void ma chiamo una action
        ///     passata in ingresso nella quale fornirà l'intero calcolato
        /// </summary>
        private void C(Action<int> continuation)
        {
            Console.WriteLine("C");
            // ... calcolo l'output intero di questo metodo
            var output = 10;
            // Passo tale output a action
            continuation(output);
        }

        /// <summary>
        ///     Metodo che ritorna un intero. Visto che siamo in CPS invece di ritornare int ritorno void ma chiamo una action
        ///     passata in ingresso nella quale fornirà l'intero calcolato
        /// </summary>
        private void D(Action<int> continuation)
        {
            Console.WriteLine("D");
            // ... calcolo l'output intero di questo metodo
            var output = 100;
            // Passo tale output a action
            continuation(output);
        }

        /// <summary>
        ///     Metodo che ritorna una stringa. Visto che siamo in CPS invece di ritornare string ritorno void ma chiamo una action
        ///     passata in ingresso nella quale fornirà la stringa calcolata.
        ///     inoltre questo metodo ha bisogno di un normale parametro in ingresso intero, <paramref name="i"/>
        /// </summary>
        private void M(int i, Action<string> continuation)
        {
            Console.WriteLine("M");
            Console.WriteLine($"L'intero passato in ingresso è {i}");
            continuation($"Stringa fornita! {i}");
        }
    }
}