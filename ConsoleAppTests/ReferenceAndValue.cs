using System.Text;

namespace ConsoleAppTests;

internal class ReferenceAndValues
{
    // va a = new Refe();
    // &a = 0x12345
    // giovanni(a)
    // &_a = 0x12345;
    internal struct IntHolder
    {
        internal int i;
    }

    internal string ReferenceTypes()
    {
        var first = new StringBuilder();
        first.Append("hello");
        var second = first; // second è un puntatore alla stessa area di memoria di first
        first.Append(" world");
        return second.ToString(); // Scrive ciò a cui punta first, cioè "hello world"
    }

    internal (string, string) ReferenceTypes2()
    {
        var first = new StringBuilder();
        first.Append("hello");
        var second = first;
        first.Append(" world");
        first = new StringBuilder("goodbye"); // ora first punta ad un nuova area di memoria
        return (first.ToString(), second.ToString()); // first scrive "goodbye"; second punta ancora all'area di memoria originale e conseguentemente scrive "hello world"
    }

    internal int ValueTypes()
    {
        var first = new IntHolder
        {
            i = 5
        };
        var second = first; // second punta ad una nuova area di memoria contenente tutti i dati di first
        first.i = 6;
        return second.i; // Scrive "5", non è influenzata dalla modifica
    }

    internal bool ReferenceTypesPassedByValue()
    {
        void Foo (StringBuilder x)
        {
            // x = new StringBuilder();
            x = null; // x ora punta a null
        }

        var y = new StringBuilder();
        y.Append ("hello");
        Foo (y);

        var b = y;
        b = new StringBuilder(); // non ha effetto su y
        return y==null;
    }

    internal string ReferenceTypesPassedByValue2()
    {
        void Foo (StringBuilder x)
        {
            x.Append (" world"); // x (che punta alla stessa area di memoria di y
        }

        var y = new StringBuilder();
        y.Append ("hello");
        Foo (y);
        return y.ToString(); // print "hello world"
    }

    internal bool ReferenceTypesPassedByReference()
    {
        void Foo (ref StringBuilder x)
        {
            x = null; // x === y, quindi impostare a null viene riflesso anche sul chiamante.
        }

        var y = new StringBuilder();
        y.Append ("hello");
        Foo (ref y);
        return y==null;
    }

    internal int ValueTypesPassedByReference()
    {
        void Foo (ref IntHolder x)
        {
            x.i=10;
        }
        var y = new IntHolder
        {
            i = 5
        };
        Foo (ref y);
        return y.i;
    }
}