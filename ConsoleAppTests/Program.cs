﻿// See https://aka.ms/new-console-template for more information

using ConsoleAppTests;

var referenceAndValues = new ReferenceAndValues();
Console.WriteLine(referenceAndValues.ReferenceTypes());
Console.WriteLine(referenceAndValues.ReferenceTypes2().Item1);
Console.WriteLine(referenceAndValues.ReferenceTypes2().Item2);
Console.WriteLine(referenceAndValues.ValueTypes());
Console.WriteLine(referenceAndValues.ReferenceTypesPassedByValue());
Console.WriteLine(referenceAndValues.ReferenceTypesPassedByValue2());
Console.WriteLine(referenceAndValues.ReferenceTypesPassedByReference());
Console.WriteLine(referenceAndValues.ValueTypesPassedByReference());
Console.ReadKey();