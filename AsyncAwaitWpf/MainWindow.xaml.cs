﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace AsyncAwaitWpfApp
{
    /// <summary>
    ///     Quando un Task è awaited il controllo ritorna subito al chiamante del metodo e verrà riesumato quando l'operazione
    ///     si concluderà.
    ///     La scrittua "async" davanti ad un metod non significa "questo metodo verrà automaticamente schedulato su un thread
    ///     asincrono" ma significa l'opposto: questo metodo contiene del codice che riguarda l'attesa di operazioni asincrone
    ///     e quindi verrà riscritto dal compilatore in modo che possa essere riesumato al momento giusto. Quindi la keyword
    ///     "async" semplicemente identifica un metodo che ha una chiamata "await" al suo interno, niente altro. I metodi
    ///     "async" rimangono nel thread corrente il più possibile, analogamente alla coroutine.
    ///     Analogamente il comando "await" non significa "questo metodo blocca il thread corrente fino a che l'operazione
    ///     asincrona viene completata, perchè altrimenti significherebbe che ritorneremmo ad una situazione sincrona che è ciò
    ///     che vogliamo evitare. Invece significa l'opposto: "await un task significa esaminare il task: se è già comkpletato
    ///     proseguire avanti come nella programmazione sincrona, altrimenti prende tutto il resto del metodo come
    ///     "continuation" (vedi la programmazione CPS) e ritornare immediatamente al chiamante. Il task chiamerà
    ///     automaticamente la sua continuation (come lambda) una volta completato.
    ///     In un metodo "async" tutto è sincrono fino al metodo "await", è lì che le cose cominciano a diventare asincrone per
    ///     davvero. La keyword "await" prende in ingresso una operazione "awaitable".
    ///     La chiave di tutto è che la keyword "await" ferma il metodo corrente fino a che il task non è completato (quindi
    ///     effettivamente aspetta) MA il thread corrente non è bloccato quindi è effettivamente asincrono.
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        ///     Il metodo OnClick deve essere con "async" in quanto al suo interno è presente la keyword "await". Dato che sono
        ///     sempre sllo stesso thread non ho problemi di accesso cross-thread per definizione.
        /// </summary>
        private async void StartButton_OnClick(object sender, RoutedEventArgs e)
        {
            // Chiamata il metodo AccessTheWebAsync e poi ritorna al chiamante, il quale è Windows. Dato che torna al chiamante non ho tutta l'interfaccia bloccata ma è tutto responsive come se fossi su un thread separato
            var contentLength = await AccessTheWebAsync();
            
            // Quando ottengo contentLengh allora posso tornare qui e scriverlo ad interfaccia
            ResultsTextBox.Text += $"\r\nLength of the downloaded string: {contentLength}.\r\n";
        }

        /// <summary>
        ///     Metodo che contiene chiamate con l'await (infatti è definito async) e che ritorna un intero
        /// </summary>
        private async Task<int> AccessTheWebAsync()
        {
            // il metodo GetStringAsync ritorna un task, ma non sono obbligato ad aspettarlo subito.
            // Questa chiamata infatti, per ora, non è per nulla asincrona. Lo sarà alla chiamata di "await"
            var getStringTask = new HttpClient().GetStringAsync("http://msdn.microsoft.com");

            // Eseguo del lavoro subito dopo la chiamata di GetStringAsync, nello stesso thread, programmazione sincrona
            DoIndependentWork();

            // Effettuo l'await del risultato di getStringTask. Quindi ho due possibilità:
            // * Se il task è già completato proseguo in modo sincrono
            // * Se il task sta ancora operando ritorno al chiamante, ritornerò qui solo quando avrà finito
            var urlContents = await getStringTask;

            // Una volta ottenuto il content dell'url, ne ritorno il valore.
            // Attenzione che dato che è un metodo awaitable anch'esso, non può ritornare un intero ma un Task<int>,
            // questo per fare in modo che anch'esso abbia tutti i vantaggi di GetStringAsync
            return urlContents.Length;
        }

        private void DoIndependentWork()
        {
            ResultsTextBox.Text += "Working . . . . . . .\r\n";
        }
    }
}