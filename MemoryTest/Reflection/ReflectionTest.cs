﻿using System.Reflection;
using BenchmarkDotNet.Attributes;

namespace Benchmarks.Reflection;
/// <summary>
/// Classe che rappresenta un mobile con un determinato colore
/// </summary>
public class Furniture
{
    /// <summary>
    ///     Property stringa che voglio andare a impostare. Il set è privato
    /// </summary>
    public string Color { get; private set; }

    /// <summary>
    ///     Metodo public: mi serve per il benchmark come riferimento senza reflection
    /// </summary>
    public void SetColor(string color)
    {
        Color = color;
    }

    /// <summary>
    ///     Metodo privato: lo utilizzo per capire cose è più veloce tra settare una property privata o metodo privato via
    ///     reflection
    /// </summary>
    private void SetColorPrivate(string color)
    {
        Color = color;
    }
}

/// <summary>
///     |                       Method |       Mean |      Error |     StdDev |
///     |----------------------------- |-----------:|-----------:|-----------:|
///     |                NonReflection |   3.450 ns |  0.2469 ns |  0.7164 ns |
///     |          ReflectionNonCached | 270.873 ns |  8.1095 ns | 23.2677 ns |
///     |             ReflectionCached | 209.192 ns | 10.0476 ns | 28.8285 ns |
///     |    ReflectionCachedWithField |  90.716 ns |  2.6844 ns |  7.7019 ns |
///     |       ReflectionWithDelegate |   5.991 ns |  0.3747 ns |  1.0752 ns |
/// </summary>
// [ShortRunJob]
public class ReflectionTest
{
    // Mettom in cache la propertyInfo in modo da non doverla calcolare tutte le volte
    private static readonly PropertyInfo ColorPropertyInfo = typeof(Furniture).GetProperty("Color")!;

    private static readonly MethodInfo SetColorPrivateMethodInfo =
        typeof(Furniture).GetMethod("SetColorPrivate", BindingFlags.NonPublic | BindingFlags.Instance)!;

    // Il set crea un backing field nascosto che vedo con le IL. Lo vado a puntare
    private static readonly FieldInfo CachedField =
        typeof(Furniture).GetField("<Color>k__BackingField", BindingFlags.Instance | BindingFlags.NonPublic)!;

    // Creo un delegare che punta al metodo setter privato della PropertyInfo in queestione.
    // Questo metodo può essere usato solo se ho a disposizione il tipo concreto (Person in questo caso) compile time.
    private static readonly Action<Furniture, string> SetColorDelegate =
        (Action<Furniture, string>)Delegate.CreateDelegate(typeof(Action<Furniture, string>),
            ColorPropertyInfo.GetSetMethod(true)!);

    // Creo un delegare che punta al metodo privato della classe in queestione.
    // Questo metodo può essere usato solo se ho a disposizione il tipo concreto (Person in questo caso) compile time.
    private static readonly Action<Furniture, string> SetColorPrivateDelegate =
        (Action<Furniture, string>)Delegate.CreateDelegate(typeof(Action<Furniture, string>),
            SetColorPrivateMethodInfo);

    private readonly Furniture _furniture = new();

    /// <summary>
    ///     Non uso alcuna reflection
    /// </summary>
    [Benchmark]
    public void NonReflection()
    {
        _furniture.SetColor("Foo");
    }

    /// <summary>
    ///     Uso la reflection calcolandomi ogni volta la property
    /// </summary>
    [Benchmark]
    public void ReflectionNonCached()
    {
        typeof(Furniture).GetProperty("Color")?.SetValue(_furniture, "Foo");
    }

    /// <summary>
    ///     Uso la reflection mettendo in cache la PropertyInfo
    /// </summary>
    [Benchmark]
    public void ReflectionCached()
    {
        ColorPropertyInfo.SetValue(_furniture, "Foo");
    }

    /// <summary>
    ///     Uso la reflection usando il backing field nascosto
    /// </summary>
    [Benchmark]
    public void ReflectionCachedWithField()
    {
        CachedField.SetValue(_furniture, "Foo");
    }

    /// <summary>
    ///     Uso un delegate per puntare al setter privato
    /// </summary>
    [Benchmark]
    public void ReflectionWithDelegate()
    {
        SetColorDelegate(_furniture, "Foo");
    }

    /// <summary>
    ///     Uso un delegate per puntare al metodo privato
    /// </summary>
    [Benchmark]
    public void ReflectionMethodWithDelegate()
    {
        SetColorPrivateDelegate(_furniture, "Foo");
    }
}