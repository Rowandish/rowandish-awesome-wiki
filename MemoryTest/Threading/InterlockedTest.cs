﻿using BenchmarkDotNet.Attributes;

namespace Benchmarks.Threading;

/// <summary>
/// |                  Method |       Mean |     Error |    StdDev |     Median | Allocated |
/// |------------------------ |-----------:|----------:|----------:|-----------:|----------:|
/// | CompareExchangeWithLock | 8,647.7 us | 152.42 us | 135.12 us | 8,646.5 us |      4 KB |
/// |         CompareExchange | 3,837.9 us |  27.82 us |  24.66 us | 3,847.4 us |      3 KB |
/// |           AddWithNoSync |   691.4 us |  12.16 us |  11.38 us |   693.2 us |      3 KB |
/// |      AddWithInterlocked | 3,567.6 us |  55.30 us |  71.91 us | 3,549.5 us |      3 KB |
/// |             AddWithLock | 9,108.2 us | 335.90 us | 990.41 us | 8,662.9 us |     12 KB |
/// </summary>
[MemoryDiagnoser]
public class InterlockedTest
{
    private readonly object _synclock = new();
    private const int Iteration = 10000_000;
    private int _interlockedValue;
    private int _lockValue;
    
    [Benchmark]
    public int CompareExchangeWithLock()
    {
        Parallel.For(0, Iteration, i =>
        {
            //lock (_synclock)
            //{
                _interlockedValue = _interlockedValue + 10 * 5 + 3; // f(_interlockedValue)
            //}
        });
        return _interlockedValue;
    }
    
    [Benchmark]
    public int CompareExchange()
    {
        Parallel.For(0, Iteration, i =>
        {
            // Utilizzo una variabile di appoggio cachedField per poter capire se qualcuno ha modificato field
            var cachedField = 0;
            var newValueFromField = 0;
            do
            {
                // Faccio puntare cachedField allo stesso indirizzo in memoria di field
                cachedField = _interlockedValue;
                // Calcolo newValueFromField in base a field. Questa è l'istruzione che voglio rendere thread-safe
                newValueFromField = cachedField + 10 * 5 + 3;
            }
            // Confronto field con cachedField. In teoria è uguale ma qualcuno potrebbe averlo modificato nel frattempo.
            // Se è stato modificato il risultato di CompareExchange è diverso da cachedField quindi non modifico field a newValueFromField e riprovo con il while; in caso contrario effettuo la modifica e esco
            while (cachedField != Interlocked.CompareExchange(ref _interlockedValue, newValueFromField, cachedField));

        });
        return _interlockedValue;
    }
    
    [Benchmark]
    public int AddWithNoSync()
    {
        Parallel.For(0, Iteration, i =>
        {
            _lockValue+=i;
            // _lockValue = _lockValue + i;
            // // ovvero:
            // var asdf = _lockValue;
            // var fdsa = asdf + i;
            // _lockValue = fdsa;
        });
        return _lockValue;
    }
    
    [Benchmark]
    public int AddWithInterlocked()
    {
        Parallel.For(0, Iteration, i =>
        {
            var asdf = _interlockedValue;
            Interlocked.Add(ref _interlockedValue, i);
            // _interlockedValue + 1 no!
        });
        return _interlockedValue;
    }
    
    [Benchmark]
    public int AddWithLock()
    {
        Parallel.For(0, Iteration, i =>
        {
            lock (_synclock)
            {
                _lockValue+=i;
            }
        });

        return _lockValue;
    }
}