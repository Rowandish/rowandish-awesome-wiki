﻿using System.Runtime.InteropServices;
using System.Security;

namespace Benchmarks.Threading
{
    /// <summary>
    ///     Consente di modificare la frequenza degli interrupt per i timer della CPU.
    ///     Di default con Windows ho 64 interrupt al secondo, ovvero uno ogni 15.625ms,
    ///     ma questo vuol dire che ogni <see cref="Thread.Sleep(int)"/> aspettera' fino
    ///     a 15ms, anche se gli dico di aspettare 1ms.
    ///
    ///     Con questa classe possiamo incrementare la risoluzione portando un interrupt
    ///     ogni millisecondo tramite il metodo <see cref="SetHighResolution"/>, sebbene
    ///     a costo di un maggior consumo di energia.
    ///
    ///     Il valore viene poi ripristinato a quello di default chiamando <see cref="ResetResolution"/>
    ///     o al termine del processo.
    ///
    ///     Vedi: https://stackoverflow.com/a/38404066/912216
    ///     Vedi: https://randomascii.wordpress.com/2020/10/04/windows-timer-resolution-the-great-rule-change/
    /// </summary>
    public static class WindowsTimerResolution
    {
        /// <summary>TimeBeginPeriod(). See the Windows API documentation for details.</summary>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("winmm.dll", EntryPoint = "timeBeginPeriod", SetLastError = true)]
        private static extern uint TimeBeginPeriod(uint uMilliseconds);

        /// <summary>TimeEndPeriod(). See the Windows API documentation for details.</summary>
        [SuppressUnmanagedCodeSecurity]
        [DllImport("winmm.dll", EntryPoint = "timeEndPeriod", SetLastError = true)]
        private static extern uint TimeEndPeriod(uint uMilliseconds);

        /// <summary>
        ///     Imposta la frequenza degli interrupt dei timer di Windows uno ogni millisecondo.
        /// </summary>
        public static void SetHighResolution()
        {
            var err = TimeBeginPeriod(1);
            const uint timerrNoerror = 0;
            if (err != timerrNoerror)
            {
            }
        }

        /// <summary>
        ///     Imposta la frequenza degli interrupt dei timer di Windows al valore di default,
        ///     ovvero 15.625ms.
        /// </summary>
        public static void ResetResolution()
        {
            var err = TimeEndPeriod(1);
            const uint timerrNoerror = 0;
            if (err != timerrNoerror)
            {
                // TIMERR_STRUCT
            }
        }
    }
}