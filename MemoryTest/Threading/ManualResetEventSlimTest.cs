﻿using BenchmarkDotNet.Attributes;

namespace Benchmarks.Threading;

/// <summary>
///     https://github.com/microsoft/referencesource/blob/master/mscorlib/system/threading/ManualResetEventSlim.cs
/// </summary>
//[ShortRunJob]
public class ManualResetEventTest
{
    [Params(0, 1, 10)] public int MillisecondsSleep;

    [GlobalSetup]
    public void GlobalSetup()
    {
        WindowsTimerResolution.SetHighResolution();
    }

    [GlobalCleanup]
    public void GlobalCleanup()
    {
        WindowsTimerResolution.ResetResolution();
    }

    /// <summary>
    ///     Creo un task che aspetta un determinato ManualResetEvent. Dopo MillisecondsSleep chiamo il metodo Set che lo
    ///     imposta a true.
    ///     Mi aspetto che con MillisecondsSleep bassi il ManualResetEventSlim sia più performante.
    /// </summary>
    [Benchmark]
    public void ManualResetEventSlim()
    {
        using var mres = new ManualResetEventSlim(false);
        var t = Task.Run(() => { mres.Wait(); });
        Thread.Sleep(MillisecondsSleep);
        mres.Set();
        t.Wait();
    }

    [Benchmark]
    public void ManualResetEvent()
    {
        using var mres = new ManualResetEvent(false);
        var t = Task.Run(() => { mres.WaitOne(); });
        Thread.Sleep(MillisecondsSleep);
        mres.Set();
        t.Wait();
    }

    /// <summary>
    ///     Verifico i tempi se il semaforo è già stato impostato a true, in teoria non dovrei avere alcun congtext swtich e
    ///     procedere diretto
    /// </summary>
    [Benchmark]
    public void ManualResetEventSlimSetWait()
    {
        using var mres = new ManualResetEventSlim(false);
        mres.Set();
        mres.Wait();
    }

    /// <summary>
    ///     Verifico i tempi se il semaforo è già stato impostato a true, in teoria non dovrei avere alcun congtext swtich e
    ///     procedere diretto
    /// </summary>
    [Benchmark]
    public void ManualResetEventSetWait()
    {
        using var mres = new ManualResetEvent(false);
        mres.Set();
        mres.WaitOne();
    }
}