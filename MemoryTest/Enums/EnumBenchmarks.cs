﻿using System.Drawing;
using BenchmarkDotNet.Attributes;
using NetEscapades.EnumGenerators;

namespace Benchmarks.Enums;
/// <summary>
/// |            Method |       Mean |        Error |    StdDev |     Median |  Gen 0 |  Gen 1 | Allocated |
/// |------------------ |-----------:|-------------:|----------:|-----------:|-------:|-------:|----------:|
/// |      EnumToString |  89.835 ns |    60.329 ns |  3.307 ns |  91.309 ns | 0.0057 | 0.0003 |      24 B |
/// |  EnumToStringFast |   1.742 ns |    36.849 ns |  2.020 ns |   1.269 ns |      - |      - |         - |
/// |     EnumIsDefined | 238.211 ns | 1,426.506 ns | 78.192 ns | 257.395 ns | 0.0057 |      - |      24 B |
/// | EnumIsDefinedFast |   3.934 ns |    32.190 ns |  1.764 ns |   4.483 ns |      - |      - |         - |
/// |      EnumTryParse | 214.551 ns |   729.410 ns | 39.981 ns | 227.536 ns |      - |      - |         - |
/// |  EnumTryParseFast |  18.513 ns |    67.781 ns |  3.715 ns |  16.517 ns |      - |      - |         - |
/// </summary>
[MemoryDiagnoser]
[ShortRunJob]
public class EnumBenchmarks
{
    [EnumExtensions]
    public enum EnumColor
    {
        ActiveBorder = 1,
        ActiveCaption,
        ActiveCaptionText,
        AppWorkspace,
        Control,
        ControlDark,
        ControlDarkDark,
        ControlLight,
        ControlLightLight,
        ControlText,
        Desktop,
        GrayText,
        Highlight,
        HighlightText,
        HotTrack,
        InactiveBorder,
        InactiveCaption,
        InactiveCaptionText,
        Info,
        InfoText,
        Menu,
        MenuText,
        ScrollBar,
        Window,
        WindowFrame,
        WindowText,
        Transparent,
        AliceBlue,
        AntiqueWhite,
        Aqua,
        Aquamarine,
        Azure,
        Beige,
        Bisque,
        Black,
        BlanchedAlmond,
        Blue,
        BlueViolet,
        Brown,
        BurlyWood,
        CadetBlue,
        Chartreuse,
        Chocolate,
        Coral,
        CornflowerBlue,
        Cornsilk,
        Crimson,
        Cyan,
        DarkBlue,
        DarkCyan,
        DarkGoldenrod,
        DarkGray,
        DarkGreen,
        DarkKhaki,
        DarkMagenta,
        DarkOliveGreen,
        DarkOrange,
        DarkOrchid,
        DarkRed,
        DarkSalmon,
        DarkSeaGreen,
        DarkSlateBlue,
        DarkSlateGray,
        DarkTurquoise,
        DarkViolet,
        DeepPink,
        DeepSkyBlue,
        DimGray,
        DodgerBlue,
        Firebrick,
        FloralWhite,
        ForestGreen,
        Fuchsia,
        Gainsboro,
        GhostWhite,
        Gold,
        Goldenrod,
        Gray,
        Green,
        GreenYellow,
        Honeydew,
        HotPink,
        IndianRed,
        Indigo,
        Ivory,
        Khaki,
        Lavender,
        LavenderBlush,
        LawnGreen,
        LemonChiffon,
        LightBlue,
        LightCoral,
        LightCyan,
        LightGoldenrodYellow,
        LightGray,
        LightGreen,
        LightPink,
        LightSalmon,
        LightSeaGreen,
        LightSkyBlue,
        LightSlateGray,
        LightSteelBlue,
        LightYellow,
        Lime,
        LimeGreen,
        Linen,
        Magenta,
        Maroon,
        MediumAquamarine,
        MediumBlue,
        MediumOrchid,
        MediumPurple,
        MediumSeaGreen,
        MediumSlateBlue,
        MediumSpringGreen,
        MediumTurquoise,
        MediumVioletRed,
        MidnightBlue,
        MintCream,
        MistyRose,
        Moccasin,
        NavajoWhite,
        Navy,
        OldLace,
        Olive,
        OliveDrab,
        Orange,
        OrangeRed,
        Orchid,
        PaleGoldenrod,
        PaleGreen,
        PaleTurquoise,
        PaleVioletRed,
        PapayaWhip,
        PeachPuff,
        Peru,
        Pink,
        Plum,
        PowderBlue,
        Purple,
        Red,
        RosyBrown,
        RoyalBlue,
        SaddleBrown,
        Salmon,
        SandyBrown,
        SeaGreen,
        SeaShell,
        Sienna,
        Silver,
        SkyBlue,
        SlateBlue,
        SlateGray,
        Snow,
        SpringGreen,
        SteelBlue,
        Tan,
        Teal,
        Thistle,
        Tomato,
        Turquoise,
        Violet,
        Wheat,
        White,
        WhiteSmoke,
        Yellow,
        YellowGreen,
        ButtonFace,
        ButtonHighlight,
        ButtonShadow,
        GradientActiveCaption,
        GradientInactiveCaption,
        MenuBar,
        MenuHighlight,
        RebeccaPurple,
    }
    
    [Benchmark]
    public string EnumToString()
    {
        return EnumColor.Aquamarine.ToString();
    }
    
    [Benchmark]
    public string EnumToStringFast()
    {
        return EnumColor.Aquamarine.ToStringFast();
    }
    
    [Benchmark]
    public bool EnumIsDefined()
    {
        return Enum.IsDefined(typeof(EnumColor), 48);
    }
    
    [Benchmark]
    public bool EnumIsDefinedFast()
    {
        return EnumColorExtensions.IsDefined((EnumColor)48);
    }
    
    [Benchmark]
    public (bool, EnumColor) EnumTryParse()
    {
        var couldParse = Enum.TryParse("Aquamarine", false, out EnumColor value);
        return (couldParse, value);
    }

    [Benchmark]
    public (bool, EnumColor) EnumTryParseFast()
    {
        var couldParse = EnumColorExtensions.TryParse("Aquamarine", false, out var value);
        return (couldParse, value);
    }
}