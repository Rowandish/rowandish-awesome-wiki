﻿using BenchmarkDotNet.Attributes;

namespace Benchmarks.Cast;
/// <summary>
/// |         Method |      Mean |     Error |    StdDev |    Median |
/// |--------------- |----------:|----------:|----------:|----------:|
/// |       HardCast | 0.5802 ns | 0.1055 ns | 0.2466 ns | 0.5162 ns |
/// |       SafeCast | 1.2295 ns | 0.0583 ns | 0.0487 ns | 1.2473 ns |
/// |      MatchCast | 1.4336 ns | 0.1837 ns | 0.5182 ns | 1.2065 ns |
///
/// |         OfType | 443.7 us | 3.92 us |  3.48 us |
/// |         CastAs | 447.3 us | 8.92 us | 14.41 us |
/// |         CastIs | 447.1 us | 6.40 us |  5.67 us |
/// |     HardCastAs | 258.3 us | 4.37 us |  6.12 us |
/// |     HardCastIs | 241.3 us | 3.73 us |  3.12 us |
/// | HardCastTypeOf | 225.5 us | 4.47 us |  4.39 us |
///
/// </summary>
// [ShortRunJob]
public class CastBenchmarks
{
    private readonly object _objectToCast = new Random();
    private readonly List<object> _listOfObjects = Enumerable.Range(0, 10_000).Select(i => (object) new Random()).ToList();

    // [Benchmark]
    public Random HardCast()
    {
        var rnd = (Random)_objectToCast;
        return rnd;
    }

    // [Benchmark]
    public Random SafeCast()
    {
        var rnd = _objectToCast as Random;
        return rnd!;
    }

    // [Benchmark]
    public Random MatchCast()
    {
        if (_objectToCast is Random rnd)
            return rnd;
        return null!;
    }

    [Benchmark]
    public List<Random> OfType()
    {
        return _listOfObjects.OfType<Random>().ToList();
    }

    [Benchmark]
    public List<Random> CastAs()
    {
        return _listOfObjects.Where(o => o as Random is not null).Cast<Random>().ToList();
    }

    [Benchmark]
    public List<Random> CastIs()
    {
        return _listOfObjects.Where(o => o is Random).Cast<Random>().ToList();
    }

    [Benchmark]
    public List<Random> HardCastAs()
    {
        return _listOfObjects.Where(o => o as Random is not null).Select(o => (Random) o).ToList();
    }

    [Benchmark]
    public List<Random> HardCastIs()
    {
        return _listOfObjects.Where(o => o is Random).Select(o => (Random) o).ToList();
    }

    [Benchmark]
    public List<Random> HardCastTypeOf()
    {
        return _listOfObjects.Where(o => o.GetType() == typeof(Random)).Select(o => (Random) o).ToList();
    }
}