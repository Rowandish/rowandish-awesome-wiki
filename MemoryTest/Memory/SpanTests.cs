﻿using BenchmarkDotNet.Attributes;

namespace Benchmarks.Memory;

//[RankColumn]
//[Orderer(SummaryOrderPolicy.FastestToSlowest)]
[MemoryDiagnoser]
[ShortRunJob]
public class SpanTests
{
    private const string TwoWords = "Foo Bar";

    [Benchmark]
    public string GetFirstString()
    {
        // Alloco un array di stringhe sullo heap, in particolare alloco "Foo" e "Bar"
        var words = TwoWords.Split(" ");
        // Prendo l'ultimo valore dell'array. Dato che le stringhe sono immutabili alloco una ulteriore sullo heap
        var firstWord = words.FirstOrDefault();
        // Ritorno la prima parola. L'array words contenente le due string che vanno out of scope in quanto perdo i puntatori dello stack.
        // Essendo variabili locali entreranno nella gen 0 del GC e al prossimo collect verranno eliminate
        return firstWord ?? string.Empty;
    }

    [Benchmark]
    public string GetFirstString2()
    {
        var words = TwoWords.Split(" ", 1);
        return words[0];
    }

    [Benchmark]
    public string GetFirstWordUsingSubstring()
    {
        // Ottengo un valore intero dell'ultimo index di ' ' (sono sullo stack)
        var lastSpaceIndex = TwoWords.LastIndexOf(" ", StringComparison.Ordinal);
        // Il metodo Substring alloca una nuova stringa sullo heap contentente "Foo"
        var firstString = TwoWords.Substring(0, lastSpaceIndex);
        return lastSpaceIndex == -1 ? string.Empty : firstString;
    }

    [Benchmark]
    public ReadOnlySpan<char> GetFirstWordUsingSpanAndLastIndexOf()
    {
        // Creo uno Span sullo stack che punta alla variabile _twoWords sullo heap
        ReadOnlySpan<char> twoStrings = TwoWords;
        //var twoStrings2 = TwoWords.AsSpan();
        // Ottengo un valore intero dell'ultimo index di ' ' (sono sullo stack)
        var lastSpaceIndex = twoStrings.LastIndexOf(' ');
        // Utilizzo Slice che è l'analogo di Substring. Aggi0ungo allo stack due variabili: offset "0" e lenght "lastSpaceIndex".
        var firstString = twoStrings.Slice(0, lastSpaceIndex);
        //var firstString = twoStrings[..lastSpaceIndex];
        return lastSpaceIndex == -1 ? ReadOnlySpan<char>.Empty : firstString;
    }
}