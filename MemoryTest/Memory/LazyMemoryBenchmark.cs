﻿using BenchmarkDotNet.Attributes;

namespace Benchmarks.Memory;

/// <summary>
/// |              Method | ArraySize |      Mean |     Error |    StdDev | Median |
/// |-------------------- |---------- |----------:|----------:|----------:|-------:|
/// |  LazyInitialization |    100000 | 0.0336 ns | 0.0387 ns | 0.1142 ns | 0.0 ns |
/// | EagerInitialization |    100000 | 0.1337 ns | 0.1087 ns | 0.3187 ns | 0.0 ns |
/// </summary>
public class LazyMemoryBenchmark
{
    /// <summary>
    /// Array Lazy
    /// </summary>
    private Lazy<int[]> lazyArray;
    /// <summary>
    /// Array Eager
    /// </summary>
    private int[] eagerArray;

    /// <summary>
    /// Dimensione dell'array
    /// </summary>
    [Params(100_000)]
    public int ArraySize { get; set; }

    /// <summary>
    /// Generare una sequenza di numeri interi da 0 a size - 1
    /// </summary>
    private static int[] GenerateArray(int size) => Enumerable.Range(0, size).ToArray();

    [GlobalSetup]
    public void GlobalSetup()
    {
        // Fornisco una factory per generare l'array. Tale factory verrà comunque chiamata una sola volta
        lazyArray = new Lazy<int[]>(() => GenerateArray(ArraySize));
        // Genero l'array all'inizio
        eagerArray = GenerateArray(ArraySize);
        // triggero che popoli una volta la cache interna in modo che questa generazione non influenzi i dati
        var value = lazyArray.Value;
        Console.WriteLine(value[0]);
    }

    [Benchmark]
    public int[] LazyInitialization()
    {
        return lazyArray.Value;
    }

    [Benchmark]
    public int[] EagerInitialization()
    {
        return eagerArray;
    }
}