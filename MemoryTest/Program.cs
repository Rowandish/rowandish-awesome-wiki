﻿using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Running;
using Benchmarks.Cast;
using Benchmarks.Enums;
using Benchmarks.Reflection;
using Benchmarks.Threading;

namespace Benchmarks;

class Program
{
    public static void Main(string[] args)
    {
        //BenchmarkRunner.Run<SpanTests>();
        // BenchmarkRunner.Run<ManualResetEventTest>();
        // BenchmarkRunner.Run<ReflectionTest>();
        //BenchmarkRunner.Run<EnumBenchmarks>();
        // BenchmarkRunner.Run<InterlockedTest>();
        BenchmarkRunner.Run<CastBenchmarks>();
        // var test = new InterlockedTest();
        // var lol = test.CompareExchange();
        // var test2 = new InterlockedTest();
        // var lol2 = test2.CompareExchangeWithLock();
        //
        // Console.WriteLine(lol);
        // Console.WriteLine(lol2);
        //
        // Console.ReadLine();

    }
}