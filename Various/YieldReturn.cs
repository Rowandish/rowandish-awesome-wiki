﻿namespace YieldReturn;

public class YieldReturn
{
    public void Run()
    {
        Console.WriteLine("Ottengo i valori di RangeZeroToFourYield");
        var rangeYield = RangeZeroToFourYield();
        Console.WriteLine("Non sono entrato nel metodo RangeZeroToFourYield!");
        Console.WriteLine("Ottengo i valori di RangeZeroToFour");
        var rangeNotYield = RangeZeroToFour();
        Console.WriteLine("Con il return normale sono entrato nel metodo RangeZeroToFour!");
        foreach (var i in rangeYield)
            Console.Write($"{i} ");
        Console.WriteLine();
        foreach (var i in rangeNotYield)
            Console.Write($"{i} ");
    }

    /// <summary>
    ///     I due metodi sono molto simili nel comportamento, ritornano una lista di numeri da 0 a 9. La differenza sta che il
    ///     metodo con lo yield return viene chiamato effettivamente solo quando viene ciclato in un foreach e fornisce un
    ///     elemento alla volta. Questo in quanto il compilatore trasforma il metodo con l'interfaccia IEnumerator in modo che
    ///     questo non venga mai chiamato fino al foreach effettivo.
    ///     Notare che non serve creare un oggetto IEnumerable, viene creato dal compilatore automaticamente
    /// </summary>
    private static IEnumerable<int> RangeZeroToFourYield()
    {
        Console.WriteLine("Entro nel metodo RangeZeroToFourYield");
        yield return 0;
        yield return 1;
        yield return 2;
        yield return 3;
        yield return 4;
    }

    /// <summary>
    ///     Metodo classico: ritorna tutta la lista. Questo metodo viene chiamato subito, indipendentemente che la lista poi
    ///     subisca un foreach o no
    /// </summary>
    private static IEnumerable<int> RangeZeroToFour()
    {
        Console.WriteLine("Entro nel metodo RangeZeroToFour");
        var output = Enumerable.Range(0, 5);
        return output;
    }
}