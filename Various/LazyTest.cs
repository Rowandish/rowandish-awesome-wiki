using System.Diagnostics;

namespace YieldReturn;

public class LazyTest
{
    public void Run()
    {
        // Utilizzo di Lazy Initialization per l'oggetto 'MyExpensiveObject'
        var lazyObject = new Lazy<MyExpensiveObject>(() => new MyExpensiveObject());
        var cronometro = Stopwatch.StartNew();

        Console.WriteLine("Lazy object definito, ora lo creo.");

        cronometro.Start();
        // L'oggetto verrà inizializzato solo quando lo richiediamo esplicitamente
        var expensiveObject = lazyObject.Value;
        cronometro.Stop();
        Console.WriteLine($"Oggetto inizializzato e pronto per l'uso ({cronometro.ElapsedMilliseconds} ms).");

        cronometro.Restart();
        expensiveObject = new MyExpensiveObject();
        cronometro.Stop();
        Console.WriteLine($"Tempo di creazione dell'oggetto senza Lazy: {cronometro.ElapsedMilliseconds} ms.");

        cronometro.Restart();
        expensiveObject = lazyObject.Value;
        cronometro.Stop();
        Console.WriteLine($"Oggetto già inizializzato, nessuna nuova inizializzazione ({cronometro.ElapsedMilliseconds} ms).\n");
    }


    private class MyExpensiveObject
    {
        public MyExpensiveObject()
        {
            Console.WriteLine("Inizializzazione dell'oggetto MyExpensiveObject...");
            // Simulazione di un'operazione costosa
            Thread.Sleep(1000);
            Console.WriteLine("Inizializzazione completata.");
        }
    }
}