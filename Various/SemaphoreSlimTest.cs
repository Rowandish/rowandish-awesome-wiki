﻿namespace YieldReturn;

public class SemaphoreSlimTest
{
    /// <summary>
    ///     Dichiaro il semaforo con 5 "biglietti": solo 5 task contemporaneamente possono accere al metodo prima di essere
    ///     bloccati.
    /// </summary>
    private readonly SemaphoreSlim _semaphore = new(5);

    /// <summary>
    ///     Lancio i 100 task
    /// </summary>
    public void Run()
    {
        Task.WaitAll(CallLongTasks().ToArray());
    }

    /// <summary>
    ///     100 task chiamano tutti il metodo <see cref="LongMethod" />. Non voglio però che più di 5 ci possano accedere
    ///     contemporanemanete.
    /// </summary>
    private IEnumerable<Task> CallLongTasks()
    {
        for (var i = 0; i < 100; i++)
            yield return LongMethod(i);
    }

    /// <summary>
    ///     Questo metodo non può essere eseguito da più di 5 task contemporaneamente.
    /// </summary>
    private async Task LongMethod(int value)
    {
        try
        {
            // Attendo che si liberi un posto
            await _semaphore.WaitAsync();
            Console.WriteLine($"{DateTime.Now:h:mm:ss:ffff} Inizio task {value}");
            // Operazione lunga: impiega un tempo casuale da 0 a 10 secondi
            await Task.Delay(new Random().Next(10_000));
            // Ho terminato l'operazione complessa, posso rilasciare il mio biglietto a qualcun altro.
            _semaphore.Release();

            Console.WriteLine($"{DateTime.Now:h:mm:ss:ffff} - Task {value} completato!");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
}