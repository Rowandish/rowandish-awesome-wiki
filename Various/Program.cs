﻿namespace YieldReturn;

public class Program
{
    /// <summary>
    ///     Questa semplice classe permette di dimostrare la differenza tra return e yield return
    /// </summary>
    public static void Main()
    {
        // new YieldReturn().Run();
        // new SemaphoreSlimTest().Run();
        new LazyTest().Run();

        Console.ReadKey();
    }
}