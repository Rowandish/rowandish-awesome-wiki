﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignPatternExamples.Behavioural.State;

public class DictionaryState
{
    private enum State
    {
        OffHook,
        Connecting,
        Connected,
        OnHold
    }

    public enum Trigger
    {
        CallDialed,
        HungUp,
        CallConnected,
        PlacedOnHold,
        TakenOffHold,
        LeftMessage
    }

    private static readonly Dictionary<State, List<(Trigger, State)>> _rules
        = new()
        {
            [State.OffHook] = new List<(Trigger, State)>
            {
                (Trigger.CallDialed, State.Connecting)
            },
            [State.Connecting] = new List<(Trigger, State)>
            {
                (Trigger.HungUp, State.OffHook),
                (Trigger.CallConnected, State.Connected)
            },
            [State.Connected] = new List<(Trigger, State)>
            {
                (Trigger.LeftMessage, State.OffHook),
                (Trigger.HungUp, State.OffHook),
                (Trigger.PlacedOnHold, State.OnHold)
            },
            [State.OnHold] = new List<(Trigger, State)>
            {
                (Trigger.TakenOffHold, State.Connected),
                (Trigger.HungUp, State.OffHook)
            }
        };

    private State _currentState = State.OffHook;

    public void MoveNext(Trigger trigger)
    {
        if (_rules[_currentState].Any(t => t.Item1 == trigger))
        {
            _currentState = _rules[_currentState].First(t => t.Item1 == trigger).Item2;
            Console.WriteLine($"Moved to {_currentState}");
        }
        else
        {
            Console.WriteLine("Invalid trigger/action for current state.");
        }
    }

    public void PrintAvailableTriggers()
    {
        Console.WriteLine($"Current state: {_currentState}");
        Console.WriteLine("Choose an action:");
        foreach (var rule in _rules[_currentState])
        {
            Console.WriteLine($"{(int)rule.Item1}. {rule.Item1}");
        }
    }
}