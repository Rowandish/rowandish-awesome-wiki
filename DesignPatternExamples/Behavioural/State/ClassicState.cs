﻿using System;

namespace DesignPatternExamples.Behavioural.State
{
    /// <summary>
    ///     Classe che identifica lo stato della classe <see cref="Context" />. Il metodo fondamentale è il metodo
    ///     <see cref="Handle" /> che *deve* avere in ingresso il <see cref="Context" /> e permette di modificarne il suo
    ///     <see cref="Context.ClassicState" /> in base a determinate condizioni.
    ///     Il trucco sta proprio nell'avere all'interno dello stato <see cref="ClassicState" /> una istanza del contesto
    ///     <see cref="Context" /> e che questo abbia una property public per poterne modificare lo stato.
    /// </summary>
    public abstract class ClassicState
    {
        public abstract void Handle(Context context);
    }

    /// <summary>
    ///     Generico stato A del <see cref="Context" />
    /// </summary>
    public class ConcreteClassicStateA : ClassicState
    {
        /// <summary>
        ///     Una volta fatte tutte le operazioni dello stato A, questo viene modificato a B se vi sono determinate condizioni.
        ///     In questo caso viene solo modificato in <see cref="ConcreteClassicStateB" />
        /// </summary>
        public override void Handle(Context context)
        {
            context.ClassicState = new ConcreteClassicStateB();
        }
    }

    /// <summary>
    ///     Generico stato B del <see cref="Context" />
    /// </summary>
    public class ConcreteClassicStateB : ClassicState
    {
        /// <summary>
        ///     Una volta fatte tutte le operazioni dello stato B, questo viene modificato a A se vi sono determinate condizioni.
        ///     In questo caso viene solo modificato in <see cref="ConcreteClassicStateA" />
        /// </summary>
        public override void Handle(Context context)
        {
            context.ClassicState = new ConcreteClassicStateA();
        }
    }

    /// <summary>
    ///     Oggetto che ha uno stato <see cref="ClassicState" /> che viene modificato nel tempo
    /// </summary>
    public class Context
    {
        /// <summary>
        ///     Stato della classe
        /// </summary>
        private ClassicState _classicState;

        /// <summary>
        ///     Costruttore: serve per inizializzare lo stato <see cref="ClassicState" />
        /// </summary>
        public Context(ClassicState classicState)
        {
            ClassicState = classicState;
        }

        /// <summary>
        ///     Metodo fondamentale, in quanto permette agli stati <see cref="ClassicState" /> di modificare lo stato del Context
        /// </summary>
        public ClassicState ClassicState
        {
            set
            {
                _classicState = value;
                Console.WriteLine($"State: {_classicState.GetType().Name}");
            }
        }

        /// <summary>
        ///     Generico metodo verso il mondo esterno. Il comportamento di questo metodo cambia in base allo stato
        ///     <see cref="_classicState" />
        /// </summary>
        public void Request()
        {
            _classicState.Handle(this);
        }
    }
}