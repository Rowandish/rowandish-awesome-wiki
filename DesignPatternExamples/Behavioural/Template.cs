﻿using System;

namespace DesignPatternExamples.Comportamentali
{
    /// <summary>
    ///     La classe astratta contiene il "template" del metodo che voglio modificare, esegue delle operazioni concrete e
    ///     delle operazioni astratte. Saranno poi le classi figlie a modificare il comportamento overridando i metodi astratti
    /// </summary>
    public abstract class Template
    {
        /// <summary>
        ///     Definisco lo scheletro dell'algoritmo
        /// </summary>
        public void TemplateMethod()
        {
            BaseOperation1();
            RequiredOperations1();
            BaseOperation2();
            Hook1();
            RequiredOperation2();
            BaseOperation3();
            Hook2();
        }

        /// <summary>
        ///     Alcuni metodi hanno già una implementazione di default, altri invece devono essere overridati
        /// </summary>
        protected void BaseOperation1()
        {
            Console.WriteLine("Template says: I am doing the bulk of the work");
        }

        protected void BaseOperation2()
        {
            Console.WriteLine("Template says: But I let subclasses override some operations");
        }

        protected void BaseOperation3()
        {
            Console.WriteLine("Template says: But I am doing the bulk of the work anyway");
        }

        /// <summary>
        ///     Questi metodi devono obbligatoriamente essere overridati per completare lo scheletro
        /// </summary>
        protected abstract void RequiredOperations1();

        protected abstract void RequiredOperation2();

        /// <summary>
        ///     Questi invece sono degli "hook": le classi figlie possono overridarli ma non è obbligatorio dato che gli hook hanno
        ///     già una implementazione, ma vuota. Gli hook permettono di estendere il comportamento dell'algoritmo template in
        ///     modo opzionale in dei punti di particolare interesse
        /// </summary>
        protected virtual void Hook1()
        {
        }

        protected virtual void Hook2()
        {
        }
    }

    /// <summary>
    ///     La classe concreta deve overridare i metodi astratti in modo obbligatorio
    /// </summary>
    internal class ConcreteClass1 : Template
    {
        protected override void RequiredOperations1()
        {
            Console.WriteLine("ConcreteClass1 says: Implemented Operation1");
        }

        protected override void RequiredOperation2()
        {
            Console.WriteLine("ConcreteClass1 says: Implemented Operation2");
        }
    }

    /// <summary>
    ///     E opzionalmente anche un hook se necessario
    /// </summary>
    internal class ConcreteClass2 : Template
    {
        protected override void RequiredOperations1()
        {
            Console.WriteLine("ConcreteClass2 says: Implemented Operation1");
        }

        protected override void RequiredOperation2()
        {
            Console.WriteLine("ConcreteClass2 says: Implemented Operation2");
        }

        protected override void Hook1()
        {
            Console.WriteLine("ConcreteClass2 says: Overridden Hook1");
        }
    }
}