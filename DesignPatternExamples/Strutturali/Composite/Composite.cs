﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternExamples.Strutturali.Composite;

public class GraphicObject
{
    /// <summary>
    ///     Ogni oggetto grafico può contenere una lista di altri oggetti analoghi a se stesso
    /// </summary>
    private readonly Lazy<List<GraphicObject>> _children = new();

    public string Color;

    /// <summary>
    ///     Utilizzo il Lazy per istanziare i children solo se ne ho bisogno
    /// </summary>
    public List<GraphicObject> Children => _children.Value;

    public virtual string Name { get; set; } = "Group";

    /// <summary>
    ///     Metodo ricorsivo per printare la depth di ogni children
    /// </summary>
    private void Print(StringBuilder sb, int depth)
    {
        sb.Append(new string('*', depth))
            .Append(string.IsNullOrWhiteSpace(Color) ? string.Empty : $"{Color} ")
            .AppendLine($"{Name}");
        foreach (var child in Children)
            child.Print(sb, depth + 1);
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        Print(sb, 0);
        return sb.ToString();
    }
}

/// <summary>
///     Notare che l'oggetto singolo che rappresenta un Circle eredita anche dall'oggetto che contiene un elenco di
///     GraphicObject, in questo modo Circle e GraphicObject che ha al suo interno una lista di Cirrcle si comportano allo
///     stesso modo
/// </summary>
public class CircleGraphicObject : GraphicObject
{
    public override string Name => "Circle";
}

public class SquareGraphicObject : GraphicObject
{
    public override string Name => "Square";
}