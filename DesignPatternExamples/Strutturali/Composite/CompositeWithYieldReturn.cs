﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DesignPatternExamples.Strutturali.Composite
{
    public static class ExtensionMethods
    {
        public static void ConnectTo(this IEnumerable<Neuron> self, IEnumerable<Neuron> other)
        {
            if (ReferenceEquals(self, other)) return;

            var otherList = other.ToList();
            foreach (var from in self)
            foreach (var to in otherList)
            {
                from.Out.Add(to);
                to.In.Add(from);
            }
        }
    }

    /// <summary>
    /// Faccio ereditare un neurone singolo da un IEnumerable di Neuron con un trucco: GetEnumerator() lo faccio yield returnare se stesso.
    /// In questo modo io posso gestire neuroni singoli come questo o insieme di neuroni come NeuronLayer allo stesso identico modo,
    /// vedi l'extension method sopra
    /// </summary>
    public class Neuron : IEnumerable<Neuron>
    {
        public float Value;
        public List<Neuron> In = new();
        public List<Neuron> Out = new();

        /// <summary>
        ///     Un oggetto singolo può "mascherarsi" da oggetto composito sfruttando lo "yield return this" sui metodi che ereditano da IEnumerable
        /// </summary>
        public IEnumerator<Neuron> GetEnumerator()
        {
            yield return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            yield return this;
        }
    }

    public class NeuronLayer : Collection<Neuron>
    {

    }
}