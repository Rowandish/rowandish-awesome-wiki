﻿using System;

namespace DesignPatternExamples.Strutturali;
// Vector2f, Vector3i

/// <summary>
///     Dato che non posso scrivere un intero nelle parentesi angolari del tipo creo un interfaccia che rappresenta un
///     numero intero
/// </summary>
public interface IInteger
{
    int Value { get; }
}

/// <summary>
///     Classe statica di comodità per contenere i numeri che mi servono
/// </summary>
public static class Dimensions
{
    /// <summary>
    ///     Rappresentazione del numero 2
    /// </summary>
    public struct Two : IInteger
    {
        public int Value => 2;
    }

    /// <summary>
    ///     Rappresentazione del numero 3
    /// </summary>
    public struct Three : IInteger
    {
        public int Value => 3;
    }
}

/// <summary>
///     Voglio creare un Vector generico: un Vector è un array di valori di tipo T di lunghezza TDigit
///     Gli obiettivi di questa classe sono molteplici:
///     Il primo è creare una classe che eredita da T e un numero. Purtroppo in c# la scrittura
///     <code>Vector&lt;T, 2&gt;</code> non è supportata quindi invece che avere un numero
///     secco passo un'interfaccia IInteger la quale ha un metodo Value che fornisce il numero che mi interessa
/// </summary>
public class Vector<TSelf, T, TDigit>
    // Non posso passare un numero, passo un interfaccia IInteger la quale ha una property Value che contiene un numero
    where TDigit : IInteger
    // Ricorsione dei tipi: in questo modo la factory Create può ritornare il tipo che ha utilizzato tale factory
    where TSelf : Vector<TSelf, T, TDigit>, new()
{
    /// <summary>
    ///     Un vector è un array di dati di tipo T con lunghezza TDigit
    /// </summary>
    private T[] _data;

    /// <summary>
    /// Costruttore che viene chiamato quando faccio new TSelf
    /// </summary>
    protected Vector()
    {
        _data = new T[default(TDigit)!.Value];
    }

    /// <summary>
    ///     In questo modo poso fare Vector[0] ed è come se facessi Vector.Data[0] ma senza rendere public Data.
    /// </summary>
    public T this[int index]
    {
        get => _data[index];
        set => _data[index] = value;
    }

    /// <summary>
    ///     Factory che permette l'inizializzazione di un nuovo oggetto con una lista di valori senza dover duplicare nei
    ///     costruttori di tutte le classi figlie (cosa che avrei dovuto fare se usassi la sintassi con il costruttore).
    ///     Ritorna TSelf in modo che se scrivo var istance = Vector3F.Create(3.5f, 2.2f, 1);, esempio, il tipo di "istance" è
    ///     "Vector3F", non un generico Vector&lt;float, Three&gt;
    ///     Questo trucco è il "recursive generic" che ho approfondito anche nei pattern Factory.
    /// </summary>
    public static TSelf Create(params T[] values)
    {
        var result = new TSelf();
        // Ottengo il numero dalla property Value di TDigit
        var requiredSize = default(TDigit)!.Value;
        // Creo un array con dimensione massima requiredSize
        result._data = new T[requiredSize];

        var providedSize = values.Length;

        // Scrivo al massimo requiredSize valori, se ne ho di più li ignoro
        for (var i = 0; i < Math.Min(requiredSize, providedSize); ++i)
            result._data[i] = values[i];

        return result;
    }
}

public class VectorOfInt<TSelf, TDigit>
    : Vector<TSelf, int, TDigit>
    where TDigit : IInteger, new()
    where TSelf : Vector<TSelf, int, TDigit>, new()
{
    public static VectorOfInt<TSelf, TDigit> operator +
        (VectorOfInt<TSelf, TDigit> lhs, VectorOfInt<TSelf, TDigit> rhs)
    {
        var result = new VectorOfInt<TSelf, TDigit>();
        var dim = new TDigit().Value;
        for (var i = 0; i < dim; i++) result[i] = lhs[i] + rhs[i];

        return result;
    }
}

public class Vector2I : VectorOfInt<Vector2I, Dimensions.Two>
{
}

public class VectorOfFloat<TSelf, TDigit>
    : Vector<TSelf, float, TDigit>
    where TDigit : IInteger, new()
    where TSelf : Vector<TSelf, float, TDigit>, new()
{
}

public class Vector3F : VectorOfFloat<Vector3F, Dimensions.Three>
{
    public void CustomMethodOfVector3F()
    {
    }
}