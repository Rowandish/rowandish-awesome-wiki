﻿namespace DesignPatternExamples.Strutturali
{
    /// <summary>
    ///     Oggetto generico che verrà utilizzato nel programma principale/chiamante. Questa interfaccia conterrà tutte i
    ///     metodi che dovranno essere chiamati dagli utilizzatori. Notare che il chiamante non sa di avere a che fare con un
    ///     AbstractDecorator, neanche nel nome.
    /// </summary>
    public interface IElement
    {
        /// <summary>
        ///     Fornisce il prezzo completo dell'elemento
        /// </summary>
        double GetPrice();

        /// <summary>
        ///     Fornisce gli ingredienti di cui è composto l'elemento
        /// </summary>
        string GetIngredients();
    }

    /// <summary>
    ///     Questa classe mi serve per poter terminare la catena dei costruttori, infatti si nota che, ereditando direttamente
    ///     da <see cref="IElement" /> invece che da <see cref="AbstractDecorator" /> non necessita di un
    ///     <see cref="IElement" /> in ingresso e conseguentemente può andare bene per terminare la catena dei
    ///     costruttori con un comportamento di default che voglio avere sempre. Per esempio se prendiamo l'esempio dei caffè,
    ///     questo oggetto sarà il caffè stesso senza nulla (che ci deve sempre essere). Le classi ElementWithXDecoration,
    ///     ElementWithYDecoration,
    ///     ElementWithZDecoration saranno invece le varie decorazioni da aggiungere all'elemento base.
    ///     Quindi anche se eredita dalla stessa interfaccia, questa classe deve essere concettualmente diversa da tutte le
    ///     altre.
    /// </summary>
    public class CoreElement : IElement
    {
        /// <summary>
        ///     Ritorna il prezzo dell'elemento base, in questo caso 1
        /// </summary>
        public double GetPrice()
        {
            return 1;
        }

        /// <summary>
        ///     Ritorna una descrizione dell'elemento base, per esempio "Main"
        /// </summary>
        /// <returns></returns>
        public string GetIngredients()
        {
            return "Main";
        }
    }

    /// <summary>
    ///     Classe cardine che permette al pattern di funzionare. Questa classe non deve essere chiamata dall'esterno, ma
    ///     verranno istanziate solo sue figlie. Il costruttore della classe, avendo in ingresso una
    ///     <see cref="IElement" /> permette la costruzione a matrioska.
    /// </summary>
    public abstract class AbstractDecorator : IElement
    {
        /// <summary>
        ///     Istanza della classe da decorare
        /// </summary>
        private readonly IElement _core;

        /// <summary>
        ///     Dato che tutte le classi figlie erediteranno da questo costruttore, avrò la possibilità di avere la costruzione new
        ///     ElementWithXDecoration(new ElementWithYDecoration(new ElementWithZDecoration....)))
        /// </summary>
        protected AbstractDecorator(IElement core)
        {
            _core = core;
        }

        /// <summary>
        ///     Delego il calcolo alle classi figlie
        /// </summary>
        public virtual double GetPrice()
        {
            return _core.GetPrice();
        }

        /// <summary>
        ///     Delego il calcolo alle classi figlie
        /// </summary>
        public virtual string GetIngredients()
        {
            return _core.GetIngredients();
        }
    }

    /// <summary>
    ///     Decorazione dell'oggetto <see cref="CoreElement" /> con l'aggiunta di "ElementWithXDecoration".
    ///     Notare come il nome non sia "XDecoration" ma "ElementWithXDecoration" in quanto, dato che
    ///     <see cref="CoreElement" /> eredita da IElement e anche la decorazione eredita dalla stessa classe, devo capire che
    ///     non sono due oggetti diversi ma lo stesso. Quindi questa non è una sola decorazione ma è l'elemento completo con la
    ///     decorazione
    /// </summary>
    public class ElementWithXDecoration : AbstractDecorator
    {
        public ElementWithXDecoration(IElement core) : base(core)
        {
        }

        public override double GetPrice()
        {
            // Potenziale computazione prima o dopo la chiamata del base
            return base.GetPrice() + 0.5;
        }

        public override string GetIngredients()
        {
            // Potenziale computazione prima o dopo la chiamata del base
            return base.GetIngredients() + ", ElementWithXDecoration";
        }
    }

    /// <summary>
    ///     Decorazione dell'oggetto <see cref="CoreElement" /> con l'aggiunta di "ElementWithYDecoration".
    /// </summary>
    public class ElementWithYDecoration : AbstractDecorator
    {
        public ElementWithYDecoration(IElement core) : base(core)
        {
        }

        public override double GetPrice()
        {
            return base.GetPrice() + 1;
        }

        public override string GetIngredients()
        {
            return base.GetIngredients() + ", ElementWithYDecoration";
        }
    }

    /// <summary>
    ///     Decorazione dell'oggetto <see cref="CoreElement" /> con l'aggiunta di "ElementWithZDecoration".
    /// </summary>
    public class ElementWithZDecoration : AbstractDecorator
    {
        public ElementWithZDecoration(IElement core) : base(core)
        {
        }

        public override double GetPrice()
        {
            return base.GetPrice() + 0.7;
        }

        public override string GetIngredients()
        {
            return base.GetIngredients() + ", ElementWithZDecoration";
        }
    }
}