﻿using System;

namespace DesignPatternExamples.Strutturali;

public interface IRenderer
{
    void RenderCircle(float radius);
}

public class VectorRenderer : IRenderer
{
    public void RenderCircle(float radius)
    {
        Console.WriteLine($"Drawing a circle of radius {radius}");
    }
}

public class RasterRenderer : IRenderer
{
    public void RenderCircle(float radius)
    {
        Console.WriteLine($"Drawing pixels for circle of radius {radius}");
    }
}

/// <summary>
/// Separo tramite un bridge l'oggetto del dominio (la shape) e il modo in cui questa viene renderizzata (interfaccia IRender) mediante l'HAS.
/// Questo pattern è molto simile al classico Strategy e risolve gli stessi problemi.
/// </summary>
public abstract class Shape
{
    protected readonly IRenderer Renderer;
    protected Shape(IRenderer renderer)
    {
        Renderer = renderer;
    }

    public abstract void Draw();
    public abstract void Resize(float factor);
}

public class Circle : Shape
{
    private float _radius;

    public Circle(IRenderer renderer, float radius) : base(renderer)
    {
        _radius = radius;
    }

    public override void Draw()
    {
        Renderer.RenderCircle(_radius);
    }

    public override void Resize(float factor)
    {
        _radius *= factor;
    }
}