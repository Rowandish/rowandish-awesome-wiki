﻿namespace DesignPatternExamples.Creationals.Prototipes;

/// <summary>
///     Creo un interfaccia che dovranno ereditare tutti gli oggeti su cui voglio poter fare una DeepCopy in modo esplicito
/// </summary>
public interface IDeepCopyable<T> where T : new()
{
  /// <summary>
  ///     Questo è l'unico metodo che dovranno implementare le classi figlie che fa effettivamente la DeepCopy dell'oggetto
  ///     target
  /// </summary>
  void CopyTo(T target);

  /// <summary>
  ///     Il codice effettivo lo posso definire anche in una interfaccia
  /// </summary>
  public T DeepCopy()
    {
        var t = new T();
        CopyTo(t);
        return t;
    }
}

public static class DeepCopyExtensions
{
  /// <summary>
  ///     Riduce duplicazioni di codice e l'utilizzo dell'interfaccia
  /// </summary>
  public static T DeepCopy<T>(this IDeepCopyable<T> item)
        where T : new()
    {
        return item.DeepCopy();
    }

  /// <summary>
  ///     Serve per poter chiamare il metodo var johnCopy = john.DeepCopy(); invece che var johnCopy = john.DeepCopy Employee
  ///     ();
  /// </summary>
  public static T DeepCopy<T>(this T person)
        where T : Person, new()
    {
        return ((IDeepCopyable<T>)person).DeepCopy();
    }
}

public class Address : IDeepCopyable<Address>
{
    public int HouseNumber;
    public string StreetName;

    public Address(string streetName, int houseNumber)
    {
        StreetName = streetName;
        HouseNumber = houseNumber;
    }

    public Address()
    {
    }

    public void CopyTo(Address target)
    {
        // Non posso usare il MemberwiseClone() quindi devo copiare tutte le property a mano
        target.StreetName = StreetName;
        target.HouseNumber = HouseNumber;
    }

    public override string ToString()
    {
        return $"{nameof(StreetName)}: {StreetName}, {nameof(HouseNumber)}: {HouseNumber}";
    }
}

public class Person : IDeepCopyable<Person>
{
    public Address Address;
    public string[] Names;

    public Person()
    {
    }

    public Person(string[] names, Address address)
    {
        Names = names;
        Address = address;
    }

    public virtual void CopyTo(Person target)
    {
        target.Names = (string[])Names.Clone();
        // La comodità di questo pattern è che è esplicito che sto facendo un DeepCopy
        target.Address = DeepCopyExtensions.DeepCopy(Address);
    }

    public override string ToString()
    {
        return $"{nameof(Names)}: {string.Join(",", Names)}, {nameof(Address)}: {Address}";
    }
}

/// <summary>
///     L'impiegato aggiunge la copia del salario al metodo CopyTo
/// </summary>
public class Employee : Person, IDeepCopyable<Employee>
{
    public int Salary;

    public void CopyTo(Employee target)
    {
        base.CopyTo(target);
        target.Salary = Salary;
    }

    public override string ToString()
    {
        return $"{base.ToString()}, {nameof(Salary)}: {Salary}";
    }
}