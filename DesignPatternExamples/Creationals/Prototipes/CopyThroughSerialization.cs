﻿using System.IO;
using System.Xml.Serialization;

namespace DesignPatternExamples.Creationals.Prototipes
{
    public static class ExtensionMethods
    {
        /// <summary>
        ///     Faccio la deep copy di un oggetto (quindi cambiandone i puntatori degli oggetti passati come riferimento) sfruttando la serializzazione a XML (in RAM ovviamente)
        /// </summary>
        public static T DeepCopy<T>(this T self)
        {
            using var ms = new MemoryStream();
            var s = new XmlSerializer(typeof(T));
            s.Serialize(ms, self);
            ms.Position = 0;
            return (T) s.Deserialize(ms);
        }
    }

    /// <summary>
    /// Questo oggetto viene deep clonato automaticamente passando per la serializzazione a XML
    /// </summary>
    public class ObjectToBeDeepCloned
    {
        public uint Stuff;
        public string Whatever;

        public override string ToString()
        {
            return $"{nameof(Stuff)}: {Stuff}, {nameof(Whatever)}: {Whatever}";
        }
    }
}