﻿using System;
using System.Collections.Generic;

namespace DesignPatternExamples.Creationals.Prototipes;

/// <summary>
///     Interfaccia dell'oggetto che voglio clonare
/// </summary>
public interface IPrototype : ICloneable
{
    /// <summary>
    ///     Guid: identificativo univoco di ogni singola istanza
    /// </summary>
    Guid Guid { get; }

    /// <summary>
    ///     Property name public modificabile dall'esterno. Tipicamente verrà fornita una nuova istanza della classe e poi sarà
    ///     il chiamante
    /// </summary>
    string Name { get; set; }
}

/// <summary>
///     Oggetto concreto di tipo "Foo"
/// </summary>
public class ConcretePrototypeFoo : IPrototype
{
    /// <summary>
    ///     Stringa costante interna
    /// </summary>
    private const string Status = "Foo Status";

    /// <summary>
    ///     Identificativo univoco che cambia ad ogni clone
    /// </summary>
    public Guid Guid { get; private set; }

    /// <summary>
    ///     Property modificabile dall'esterno
    /// </summary>
    public string Name { get; set; } = "Foo Name";

    /// <summary>
    ///     Effettuo il Clone modificando anche il Guid interno
    /// </summary>
    public object Clone()
    {
        var clone = (ConcretePrototypeFoo)MemberwiseClone();
        clone.Guid = Guid.NewGuid();
        return clone;
    }

    public override string ToString()
    {
        return $"Name: {Name} - Status: {Status} - Guid: {Guid}";
    }
}

/// <summary>
///     Oggetto concreto di tipo "Bar"
/// </summary>
public class ConcretePrototypeBar : IPrototype
{
    /// <summary>
    ///     Stringa costante interna
    /// </summary>
    private const string Status = "Bar Status";

    /// <summary>
    ///     Identificativo univoco che cambia ad ogni clone
    /// </summary>
    public Guid Guid { get; private set; }

    /// <summary>
    ///     Property modificabile dall'esterno
    /// </summary>
    public string Name { get; set; } = "Bar Name";

    /// <summary>
    ///     Effettuo il Clone modificando il Guid interno
    /// </summary>
    public object Clone()
    {
        var clone = (ConcretePrototypeBar)MemberwiseClone();
        clone.Guid = Guid.NewGuid();
        return clone;
    }

    public override string ToString()
    {
        return $"Name: {Name} - Status: {Status} - Guid: {Guid}";
    }
}

/// <summary>
///     Factory che permette la creazioni di classi di tipo <see cref="IPrototype" /> clonando il prototype base ogni volta
///     che viene richiesta una nuova istanza. Invece di creare ogni volta una classe da zero parto già da un prototipo con
///     tutti i suoi campi e poi eventualmente effettuo delle modifiche alle property che mi servono
/// </summary>
public class PrototypeFactory
{
    /// <summary>
    ///     Dizionario contenente le istanze base dei prototipi che posso creare. Quando mi viene richiesto un nuovo prototipo
    ///     parto da questi e ne effettuo il Clone() senza che il mondo esterno se ne accorga
    /// </summary>
    private static readonly IDictionary<RecordType, IPrototype> Prototypes = new Dictionary<RecordType, IPrototype>();

    /// <summary>
    ///     Costruttore: crea le istanze di tutti i prototipi che poi andrò a creare
    /// </summary>
    public PrototypeFactory()
    {
        Prototypes.Add(RecordType.Foo, new ConcretePrototypeFoo());
        Prototypes.Add(RecordType.Bar, new ConcretePrototypeBar());
    }

    /// <summary>
    ///     Fornisce una nuova istanza di una classe di tipo <see cref="IPrototype" /> partendo dal <paramref name="type" />
    ///     passato in ingresso
    /// </summary>
    public IPrototype CreatePrototype(RecordType type)
    {
        return Prototypes[type].Clone() as IPrototype;
    }
}

/// <summary>
///     Tipologie di classi <see cref="IPrototype" /> che posso creare
/// </summary>
public enum RecordType
{
    Foo,
    Bar
}