﻿using System;

namespace DesignPatternExamples.Creationals.Factories;

/// <summary>
///     Il problema dei costruttori è che non possono esistere due costruttori con gli stessi argomenti con lo stesso tipo.
///     Per esempio assumiamo di voler creare la classe Point che ha a costruttore x e y. Se le coordinate sono cartesiane
///     va bene così, se invece sono polari questi numeri fanno moltiplicati per dei coseni. Per poter comunicare questa
///     cosa all'esterno in modo che sia esplicito il tipom di classe che sto creando posso utilizzare il pattern Factory
///     rendendo privato il costruttore e costruendo il mio oggetto con una factory public all'internod della classe.
///     La factory deve essere all'interno della classe al fine di tenere il costruttore privato
/// </summary>
public class Point
{
    /// <summary>
    /// Factory interna alla classe in modo da poter accere al costruttore di Point anche se privato
    /// </summary>
    public class PointFactory
    {
        /// <summary>
        ///     Factory method for creating a point using Cartesian coordinates
        /// </summary>
        public static Point CreateFromCartesianCoordinates(double x, double y)
        {
            return new Point(x, y);
        }

        /// <summary>
        ///     Factory method for creating a point using polar coordinates
        /// </summary>
        public static Point CreateFromPolarCoordinates(double radius, double angleInRadians)
        {
            var x = radius * Math.Cos(angleInRadians);
            var y = radius * Math.Sin(angleInRadians);
            return new Point(x, y);
        }
    }

    public double X { get; set; }
    public double Y { get; set; }

    /// <summary>
    ///     Dall'esterno non voglio che venga chiamato questo costruttore, fornisco io dei metodi factory statici
    /// </summary>
    private Point(double x, double y)
    {
        X = x;
        Y = y;
    }
}