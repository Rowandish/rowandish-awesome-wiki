﻿namespace DesignPatternExamples.Creationals.Factories
{
    /// <summary>
    ///     Il pattern AbstracFactory è il pattern principe per ottenere il principio di inversione delle dipendeze (DIP).
    ///     Questo principio indica che la massima flessibilità dei sistemi si ottiene quando le dipendenze del codice sorgente
    ///     fanno riferimento solo ad astrazioni e non a concrezioni.
    ///     Per esempio nella mia applicazione voglio usare "ConcreteProductA1". Ora, io potrei fare AbstractProductA prod =
    ///     new ConcreteProductA1(), il tipo di dato è astratto (bene) ma la sua istanza è concreta. Il risultato è accettabile
    ///     ma ho comunque che la mia classe dipende da una classe concreta, ne effettua il new.
    ///     Per risolvere questo problema posso fare in modo che il new ConcreteProductA1() venga effettuato da una factory
    ///     concreta cui cui io non ho il controllo.
    ///     Quello che otterrò alla fine sarà IFactoryProduct _abstractProductA = factory.CreateProductA(), quindi nel
    ///     programma non ho alcuna dipendenza da nulla di concreto.
    ///     Ho quindi la divisone del sistema in due componenti, uno concreto e uno astratto. Il componente astratto contiene
    ///     tutte le regole operative di alto livello dell'applicazione, il componente concreto contiene tutti i dettagli
    ///     implementativi manipolati da tali regole operative.
    ///     Metodo factory astratto. E' astratto in quanto i metodi factory dipendono dalle sue implementazioni concrete
    /// </summary>
    internal interface IAbstractFactory
    {
        /// <summary>
        ///     Factory per creare l'oggetto <see cref="T:DesignPatternExamples.Creationals.Factories.IFactoryProduct" /> di tipo A
        /// </summary>
        IFactoryProduct CreateProductA();

        /// <summary>
        ///     Factory per creare l'oggetto <see cref="T:DesignPatternExamples.Creationals.Factories.IFactoryProduct" /> di tipo B
        /// </summary>
        IFactoryProduct CreateProductB();
    }


    /// <summary>
    ///     Esempio di Factory concreta, questa factory di occupa di creare le istanze concrete. Con questo pattern ho una
    ///     perfetta scorporazione in quanto il client non ha visibilità nemmeno sull'oggetto concreto che sta creando
    /// </summary>
    internal class ConcreteFactory1 : IAbstractFactory
    {
        public virtual IFactoryProduct CreateProductA()
        {
            return new ConcreteProductA1();
        }

        public virtual IFactoryProduct CreateProductB()
        {
            return new ConcreteProductB1();
        }
    }

    /// <summary>
    ///     Esempio di Factory concreta, questa factory di occupa di creare le istanze concrete. Con questo pattern ho una
    ///     perfetta scorporazione in quanto il client non ha visibilità nemmeno sull'oggetto concreto che sta creando
    /// </summary>
    internal class ConcreteFactory2 : IAbstractFactory
    {
        public virtual IFactoryProduct CreateProductA()
        {
            return new ConcreteProductA2();
        }

        public virtual IFactoryProduct CreateProductB()
        {
            return new ConcreteProductB2();
        }
    }

    /// <summary>
    ///     Generico prodotto B astratto
    /// </summary>
    internal interface IFactoryProduct
    {
        string Interact(IFactoryProduct a);
    }

    internal abstract class AbstractProductA : IFactoryProduct
    {
        public virtual string Interact(IFactoryProduct a)
        {
            return $"Product A: {GetType().Name} interacts with {a.GetType().Name}";
        }
    }

    /// <summary>
    ///     Prorotto concreto A2
    /// </summary>
    internal class ConcreteProductA2 : AbstractProductA

    {
    }


    /// <summary>
    ///     Prorotto concreto A1
    /// </summary>
    internal class ConcreteProductA1 : AbstractProductA

    {
    }

    internal abstract class AbstractProductB : IFactoryProduct
    {
        public virtual string Interact(IFactoryProduct a)
        {
            return $"Product B: {GetType().Name} interacts with {a.GetType().Name}";
        }
    }


    /// <summary>
    ///     Prorotto concreto B1
    /// </summary>
    internal class ConcreteProductB1 : AbstractProductB

    {
    }

    /// <summary>
    ///     Prorotto concreto B2
    /// </summary>
    internal class ConcreteProductB2 : AbstractProductB

    {
    }


    /// <summary>
    ///     Oggetto che utilizza gli oggetti creati dalla factory <see cref="IFactoryProduct" />
    /// </summary>
    internal class Client
    {
        private readonly IFactoryProduct _productA;
        private readonly IFactoryProduct _productB;

        /// <summary>
        ///     Data la factory <paramref name="factory" /> in ingresso ho la creazione dei prodotti utilizzati dalla classe
        /// </summary>
        public Client(IAbstractFactory factory)
        {
            // N.B. il client lavora con tipi astratti e inoltre non conosce l'istanza dell'oggetto creato in quanto questo dipende dalla factory.
            // In questo modo il client è aperto alle estensioni ma chiuso alle modifiche: qualsiasi cosa accada al mondo concreto questo codice non cambia
            _productA = factory.CreateProductA();
            _productB = factory.CreateProductB();
        }

        // Il problema del costruttore sopra è che utilizzo la classe factory all'interno del client con la chiamata ad un
        // metodo specifico: se la factory con il metodo CreateProductA() crea l'oggetto A, se ho un nuovo oggetto B devo modificare
        // la classe Client con CreateProductB(), portando quindi ad un accoppiamento, seppur minimo.
        // L'obiettivo è di non dover mai modificare la classe Client, nemmeno la chiamata alla factory.
        // E' ancora meglio quidni passare direttamente gli oggetti a costruttore e delegare la loro creazione a fuori, così ho il minimo accoppiamento possibile.
        public Client(IFactoryProduct productA, IFactoryProduct productB)
        {
            _productB = productA;
            _productA = productB;
        }

        /// <summary>
        ///     Generico metodo per dimostrare che tutto funziona
        /// </summary>
        public string Run()
        {
            return _productB.Interact(_productA);
        }
    }
}