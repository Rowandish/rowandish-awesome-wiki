﻿using System.Threading.Tasks;

namespace DesignPatternExamples.Creationals.Factories;

/// <summary>
///     L'obiettivo di questa classe è costruire un oggetto chiamando metodi async, cosa che non posso fare in un
///     costruttore normale. Costruisco quindi un metodo factory public async e obbligo il chiamante a chiamare tale
///     oggetto per costruire l'oggetto
/// </summary>
public class AsyncFactoryUser
{
    public string Username { get; private set; }
    public bool IsAuthenticated { get; private set; }

    /// <summary>
    /// Costruttore privato per evitare la creazione di istanze della classe direttamente.
    /// In questgo costruttore non posso chiamare nulla di asincrono
    /// </summary>
    private AsyncFactoryUser(string username, bool isAuthenticated)
    {
        Username = username;
        IsAuthenticated = isAuthenticated;
    }

    /// <summary>
    ///     Metodo factory asincrono che permette di creare l'utente ma solo dopo una chiamata asincrona.
    /// </summary>
    public static async Task<AsyncFactoryUser> CreateUserAsync(string username)
    {
        // Chiamata asincrona per verificare l'autenticazione
        var isAuthenticated = await VerifyAuthenticationAsync(username);

        return new AsyncFactoryUser(username, isAuthenticated);
    }

    /// <summary>
    /// Metodo asincrono per verificare l'autenticazione
    /// </summary>
    private static async Task<bool> VerifyAuthenticationAsync(string username)
    {
        // Simulazione di un'operazione asincrona di verifica dell'autenticazione
        await Task.Delay(500); // Simulazione di mezzo secondo di attesa

        // Qui potresti avere la tua logica reale per verificare l'autenticazione
        // In questo esempio, verrà restituito true se il nome utente non è vuoto
        return !string.IsNullOrEmpty(username);
    }
}