﻿using System;
using System.Collections.Generic;

namespace DesignPatternExamples.Creationals.Factories;

public interface ITheme
{
    string TextColor { get; }
    string BgrColor { get; }
}

internal class LightTheme : ITheme
{
    public string TextColor => "black";
    public string BgrColor => "white";
}

internal class DarkTheme : ITheme
{
    public string TextColor => "white";
    public string BgrColor => "dark gray";
}

/// <summary>
///     In questo caso utilizzo il metodo Factory al fine di tenermi in memoria tutti gli oggetti che sono stati creati al
///     fine di poterli sostituire in una volta sola senza che il mondo esterno lo sappia
/// </summary>
public class ReplaceableThemeFactory
{
    /// <summary>
    ///     Mi salvo la lista dei temi che ho creato.
    ///     Utilizzo WeakReference in quanto non voglio interferire con il GC, se un tema non serve a nessuno il GC lo può
    ///     eliminare. Utilizzo la classe Ref in modo che abbia una classe wrapper con un puntatore da sostituire comodamente
    /// </summary>
    private readonly List<WeakReference<Ref<ITheme>>> _themes = new();

    /// <summary>
    ///     Metodo privato che effettivamente costruisce l'oggetto in base al booleano in ingresso
    /// </summary>
    private static ITheme CreateThemeImpl(bool dark)
    {
        return dark ? new DarkTheme() : new LightTheme();
    }

    /// <summary>
    ///     Metodo factory per costruire l'oggetto e tenermi una weakreference dell'oggetto appena creato
    /// </summary>
    public Ref<ITheme> CreateTheme(bool dark)
    {
        CleanUpThemes();
        var r = new Ref<ITheme>(CreateThemeImpl(dark));
        _themes.Add(new WeakReference<Ref<ITheme>>(r));
        return r;
    }

    /// <summary>
    ///     Modifico i puntatori a tutti gli oggetti che ho creato. Il mondo esterno avrà tutti i puntatori sostituiti senza
    ///     che se ne accorga
    /// </summary>
    public void ReplaceTheme(bool dark)
    {
        CleanUpThemes();
        foreach (var wr in _themes)
            if (wr.TryGetTarget(out var reference))
                reference.Value = CreateThemeImpl(dark);
    }

    /// <summary>
    ///     Rimuove tutte le WeakReference dalla lista _themes che non hanno più un oggetto valido per evitare che la lista
    ///     diventi sempre più grande all'infinito
    /// </summary>
    private void CleanUpThemes()
    {
        _themes.RemoveAll(wr => !wr.TryGetTarget(out _));
    }
}

public class Ref<T> where T : class
{
    public T Value; // TODO sul set scattare un evento

    public Ref(T value)
    {
        Value = value;
    }
}