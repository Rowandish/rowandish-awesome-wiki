﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DesignPatternExamples.Creationals.Singletons
{
    /// <summary>
    /// Questa classe permette di costruire dei singleton ma per ogni thread. Quindi a parità di thread avrò sempre la stessa istanza mentre questa verrà modificata se sono in un nuovo thread
    /// </summary>
    public sealed class PerThreadSingleton
    {
        private static readonly ThreadLocal<PerThreadSingleton> ThreadInstance = new(() => new PerThreadSingleton());

        public readonly int Id;

        private PerThreadSingleton()
        {
            Id = Environment.CurrentManagedThreadId;
        }

        public static PerThreadSingleton Instance => ThreadInstance.Value;
    }
}