﻿using System;
using System.Collections.Generic;

namespace DesignPatternExamples.Creationals.Singletons;

/// <summary>
///     L'obiettivo è avere un punto dove accedere a delle variabili globali di ambiente, che sono le stesse per tutto il
///     codice indipentemente dalle classi che lo utilizzano.
///     Inoltre vorrei creare uno scope per queste variabili, per cui in un determinato scope le variabili valgono X ma
///     fuori da tale scope assumono un valore diverso. Se fossero sempre uguali utilizzo la buona vecchia variabile
///     statica stile Math.PI e fine.
/// </summary>
public sealed class AmbientContext : IDisposable
{
    private const int StandardAmbientVariableValue = 123;

    /// <summary>
    ///     Pila dei context: quello che utilizzo è sempre l'ultimo e una volta che l'ho utilizzato lo rimuovo dallo stack per
    ///     usare il precedente
    /// </summary>
    private static readonly Stack<AmbientContext> ContextsStack = new();

    /// <summary>
    ///     Questa è la variabile di ambiente ma il cui valore può essere cambiato in un determinato scope
    /// </summary>
    public readonly int AmbientVariableToBeChanged;

    /// <summary>
    ///     Un costruttore statico viene chiamato automaticamente una sola volta quando la classe viene caricata per la prima
    ///     volta nel processo. Questo popola con un elemento ContextsStack il quale è anch'esso static, quindi non cambia mai
    ///     per ogni istanza di questa classe. Questa scrittura permette di avere uno stack condiviso con tutte le istanze di
    ///     AmbientContext in cui sicuramente c'è almeno un elemento, che è il context di default
    /// </summary>
    static AmbientContext()
    {
        // Ho almeno un context con il valore standard
        ContextsStack.Push(new AmbientContext(StandardAmbientVariableValue));
    }

    /// <summary>
    ///     Costruttore standard per creare un nuovo AmbientContext da mettere nello stack ContextsStack. Fino al Dispose
    ///     l'accesso a Current fornirà questo context.
    /// </summary>
    public AmbientContext(int ambientVariableToBeChanged)
    {
        AmbientVariableToBeChanged = ambientVariableToBeChanged;
        ContextsStack.Push(this);
    }

    /// <summary>
    ///     Il context corrente è l'ultimo dello stack
    /// </summary>
    public static AmbientContext Current => ContextsStack.Peek();

    /// <summary>
    ///     Il dispose di questa classe rimuove l'ultimo elemento dello stack tornando al precedente
    /// </summary>
    public void Dispose()
    {
        if (ContextsStack.Count > 1)
            ContextsStack.Pop();
    }
}