﻿using System;
using System.Collections.Generic;
using System.IO;
using static System.Console;

namespace DesignPatternExamples.Creationals.Singletons
{
  public interface IGenericInterface
  {
  }

  public class SingletonObject : IGenericInterface
  {
    public static int Count { get; private set; }

    private SingletonObject()
    {
    }

    // laziness + thread safety
    private static readonly Lazy<SingletonObject> PrivateInstance = new(() =>
    {
      Count++;
      return new SingletonObject();
    });

    public static IGenericInterface Instance => PrivateInstance.Value;
  }
}
