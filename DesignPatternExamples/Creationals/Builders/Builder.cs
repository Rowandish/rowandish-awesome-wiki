﻿using System.Collections.Generic;

namespace DesignPatternExamples.Creationals.Builders;

/// <summary>
///     Il pattern builder  separa la costruzione di un oggetto complesso dalla sua rappresentazione, in modo che il
///     processo di costruzione stesso possa creare diverse rappresentazioni.
///     Ciò ha l'effetto immediato di rendere più semplice la classe, permettendo a una classe builder separata di
///     focalizzarsi sulla corretta costruzione di un'istanza e lasciando che la classe originale si concentri sul
///     funzionamento degli oggetti.
///     Il pattern builder si può riconoscere in una classe che ha un solo metodo di creazione e vari metodi per
///     configurare l'oggetto creato.
///     Differenze con il Factory: il Builder si focalizza sulla costruzione di un oggetto complesso "step by step".
///     Factory enfatizza una famiglia di oggetti (sia semplici che complessi).
///     Il Builder restituisce il prodotto come passo finale del processo di creazione, mentre per quanto riguarda
///     l'Abstract Factory, il prodotto viene ritornato immediatamente.
/// </summary>
public abstract class AbstractBuilder
{
    protected Product Product = new();

    /// <summary>
    ///     Costruttore, istanzia un nuovo <see cref="Product" />
    /// </summary>
    protected AbstractBuilder()
    {
    }

    /// <summary>
    ///     Crea una nuova istanza di <see cref="Product" />
    /// </summary>
    public void CreateNewProduct()
    {
        Product = new Product();
    }

    /// <summary>
    ///     Costruisco la parte A dell'oggetto
    /// </summary>
    public abstract void BuildPartA();

    /// <summary>
    ///     Costruisco la parte B dell'oggetto
    /// </summary>
    public abstract void BuildPartB();

    /// <summary>
    ///     Costruisco la parte C dell'oggetto
    /// </summary>
    public abstract void BuildPartC();

    /// <summary>
    ///     Questo metodo fornisce il prodotto costruito. Opzionalmente posso chiamare anche il metodo
    ///     <see cref="CreateNewProduct" /> che
    ///     annulla l'istanza; dipende dalle esigenze
    /// </summary>
    public Product GetProduct()
    {
        var result = Product;

        CreateNewProduct();

        return result;
    }
}

/// <summary>
///     Costruisce e assembla le parti del prodotto implementando l'interfaccia Builder; definisce e tiene traccia della
///     rappresentazione che crea.
///     Il builder concreto è figlio dell'interfaccia e fornisce le implementazioni dei vari step, in questo modo posso
///     avere n builder diversi che producono lo stesso oggetto.
/// </summary>
public class ConcreteBuilder : AbstractBuilder
{
    /// <summary>
    ///     Tutti i metodi per la creazione dell'oggetti utilizzano la stessa istanza di <see cref="Product" />
    /// </summary>
    public override void BuildPartA()
    {
        Product.Add("Standard PartA1");
    }

    public override void BuildPartB()
    {
        Product.Add("Standard PartB1");
    }

    public override void BuildPartC()
    {
        Product.Add("Standard PartC1");
    }
}

/// <summary>
///     Costruisce e assembla le parti del prodotto implementando l'interfaccia Builder; definisce e tiene traccia della
///     rappresentazione che crea.
///     Il builder concreto è figlio dell'interfaccia e fornisce le implementazioni dei vari step, in questo modo posso
///     avere n builder diversi che producono lo stesso oggetto.
/// </summary>
public class CustomBuilder : AbstractBuilder
{
    /// <summary>
    ///     Tutti i metodi per la creazione dell'oggetti utilizzano la stessa istanza di <see cref="Product" />
    /// </summary>
    public override void BuildPartA()
    {
        Product.Add("Custom PartA1");
    }

    public override void BuildPartB()
    {
        Product.Add("Custom PartB1");
    }

    public override void BuildPartC()
    {
        Product.Add("Custom PartC1");
    }
}

/// <summary>
///     Ha senso usare il pattern Builder solo quando l'oggetto che voglio creare è molto complesso e necessita di molta
///     configurazione che non voglio fare all'esterno.
/// </summary>
public class Product
{
    private readonly IList<string> _parts = new List<string>();

    public void Add(string part)
    {
        _parts.Add(part);
    }

    public string ListParts()
    {
        return $"Product parts: {string.Join(", ", _parts)}";
    }
}

/// <summary>
///     Il Director costruisce un oggetto utilizzando l'interfaccia Builder, infatti notifica al Builder se una parte
///     del prodotto deve essere costruita, il Builder riceve le richieste dal Director e aggiunge le parti al prodotto.
///     Lo scopo del Director è eseguire i passi della costruzione dell'oggetto in un particolare ordine, o solo alcuni
///     passi, in base ai metodi chiamati dall'esterno.
///     In questo caso <see cref="BuildMinimalViableProduct" /> chiamerà solo un metodo del Builder, mentre
///     <see cref="BuildFullFeaturedProduct" /> chiamerà tutti i metodi.
///     Questa classe non è obbligatoria nel pattern in quanto il client può fare la stessa operazione, ha senso solo se ho
///     varie configurazioni diverse dello stesso oggetto da costruire.
/// </summary>
public class Director
{
    /// <summary>
    ///     Costruttore: imposta il <paramref name="builder" /> iniziale
    /// </summary>
    public Director(AbstractBuilder builder)
    {
        Builder = builder;
    }

    /// <summary>
    ///     Builder impostabile dall'esterno. Obbligo il passaggio anche a costruttore in quanto è obbligatorio avere almeno un
    ///     builder impostato
    /// </summary>
    public AbstractBuilder Builder { get; set; }

    /// <summary>
    ///     Delega al builder la costruione di un nuovo <see cref="Product" />, in questo caso creando tutte le parti
    /// </summary>
    public void BuildFullFeaturedProduct()
    {
        Builder.CreateNewProduct();
        Builder.BuildPartA();
        Builder.BuildPartB();
        Builder.BuildPartC();
    }

    /// <summary>
    ///     Delega al builder la costruione di un nuovo <see cref="Product" /> ma creando solo la parte A
    /// </summary>
    public void BuildMinimalViableProduct()
    {
        Builder.CreateNewProduct();
        Builder.BuildPartA();
    }

    /// <summary>
    ///     Fornisce il <see cref="Product" /> costruito al Client
    /// </summary>
    public Product GetProduct()
    {
        return Builder.GetProduct();
    }
}