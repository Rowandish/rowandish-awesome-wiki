﻿namespace DesignPatternExamples.Creationals.Builders;

/// <summary>
///     Il pattern Fluent Builder estende il concetto di Builder permettendo una catena di chiamate ai metodi (method chaining),
///     migliorando la leggibilità e la facilità di utilizzo per la costruzione di oggetti complessi. Questo pattern è particolarmente
///     utile quando l'oggetto da costruire necessita di una configurazione dettagliata e/o ha molti attributi che possono essere
///     configurati in modi diversi.
/// </summary>
public abstract class FluentAbstractBuilder
{
    protected Product Product = new();

    /// <summary>
    ///     Inizializza una nuova istanza di <see cref="Product"/>.
    /// </summary>
    protected FluentAbstractBuilder()
    {
        CreateNewProduct();
    }

    /// <summary>
    ///     Reinizializza <see cref="Product"/> per una nuova costruzione.
    /// </summary>
    public FluentAbstractBuilder CreateNewProduct()
    {
        Product = new Product();
        return this;
    }

    /// <summary>
    ///     Costruisce la parte A dell'oggetto e ritorna il builder per ulteriori configurazioni.
    /// </summary>
    public abstract FluentAbstractBuilder BuildPartA();

    /// <summary>
    ///     Costruisce la parte B dell'oggetto e ritorna il builder per ulteriori configurazioni.
    /// </summary>
    public abstract FluentAbstractBuilder BuildPartB();

    /// <summary>
    ///     Costruisce la parte C dell'oggetto e ritorna il builder per ulteriori configurazioni.
    /// </summary>
    public abstract FluentAbstractBuilder BuildPartC();

    /// <summary>
    ///     Restituisce il prodotto costruito.
    /// </summary>
    public Product GetProduct()
    {
        return Product;
    }
}

/// <summary>
///     Implementazione concreta di <see cref="FluentAbstractBuilder"/> che costruisce e assembla le parti del prodotto.
///     Le implementazioni specifiche dei metodi consentono la creazione di configurazioni del prodotto personalizzate.
/// </summary>
public class FluentConcreteBuilder : FluentAbstractBuilder
{
    public override FluentAbstractBuilder BuildPartA()
    {
        Product.Add("Fluent PartA");
        return this;
    }

    public override FluentAbstractBuilder BuildPartB()
    {
        Product.Add("Fluent PartB");
        return this;
    }

    public override FluentAbstractBuilder BuildPartC()
    {
        Product.Add("Fluent PartC");
        return this;
    }
}

/// <summary>
///     Il Director costruisce un oggetto utilizzando il Fluent Builder, gestendo la sequenza di costruzione e ottenendo il prodotto finale.
///     Il client può scegliere di costruire configurazioni diverse dello stesso oggetto tramite una serie di chiamate concatenate.
/// </summary>
public class FluentDirector
{
    private readonly FluentAbstractBuilder _builder;

    /// <summary>
    ///     Costruttore: imposta il builder iniziale.
    /// </summary>
    public FluentDirector(FluentAbstractBuilder builder)
    {
        _builder = builder;
    }

    /// <summary>
    ///     Costruisce e fornisce un prodotto completo.
    /// </summary>
    public Product BuildFullFeaturedProduct()
    {
        return _builder.CreateNewProduct()
            .BuildPartA()
            .BuildPartB()
            .BuildPartC()
            .GetProduct();
    }

    /// <summary>
    ///     Costruisce e fornisce un prodotto con le funzionalità minime.
    /// </summary>
    public Product BuildMinimalViableProduct()
    {
        return _builder.CreateNewProduct()
            .BuildPartA()
            .GetProduct();
    }
}