﻿using System;

namespace DesignPatternExamples.Creationals.Builders;

/// <summary>
///     Rappresenta una persona con nome, posizione professionale e data di nascita.
/// </summary>
public class Person
{
    public DateTime DateOfBirth;
    public string Name;
    public string Position;

    /// <summary>
    ///     Fornisco all'esterno l'oggetto builder a cui verranno chiamati i metodi.
    /// </summary>
    public static Builder New => new();

    /// <summary>
    ///     Override del metodo ToString per rappresentare l'oggetto Person come stringa.
    /// </summary>
    public override string ToString()
    {
        return
            $"{nameof(Name)}: {Name}, {nameof(Position)}: {Position}, {nameof(DateOfBirth)}: {DateOfBirth.ToShortDateString()}";
    }

    /// <summary>
    ///     Builder interno che eredita da PersonBirthDateBuilder per costruire un oggetto Person.
    /// </summary>
    public class Builder : PersonBirthDateBuilder<Builder>
    {
        internal Builder()
        {
        }
    }
}

/// <summary>
///     Classe astratta base per la costruzione di un oggetto Person.
/// </summary>
public abstract class AbstractPersonBuilder
{
    /// <summary>
    /// Oggetto che sto costruendo
    /// </summary>
    protected readonly Person Person = new();

    /// <summary>
    ///     Fornisce l'oggetto appena costruito
    /// </summary>
    public Person Build()
    {
        return Person;
    }
}

/// <summary>
///     Builder generico per l'impostazione del nome di una persona.
///     Utilizza i generics in modo ricorsivo (where TSelf : AbstractPersonInfoBuilder) al fine di poter fare builder uno figlio dell'altro
///     che si concatenano l'un l'altro senza errori di compilazione.
///     Il where serve ovviamente per evitare che l'utente passi il tipo che vuole ma forzare che sia sempre un builder astratto.
/// </summary>
public class AbstractPersonInfoBuilder<TSelf> : AbstractPersonBuilder where TSelf : AbstractPersonInfoBuilder<TSelf>
{
    /// <summary>
    ///     Imposta il nome della persona e ritorna l'istanza del builder per permettere il method chaining.
    /// </summary>
    public TSelf Called(string name)
    {
        Person.Name = name;
        // Nono posso ritornare l'oggetto corrente ma l'oggetto che viene passato tramite l'ereditgarietà
        return (TSelf)this;
    }
}

/// <summary>
///     Voglio creare un builder che sia figlio di AbstractPersonInfoBuilder.
///     Il problema è che così facendo non posso ritornare this come nel classico FluentBuilder in quanto io sono suo figlio, uso il trucco dei generics ricorsivi.
/// </summary>
public class PersonPositionJobBuilder<TSelf> : AbstractPersonInfoBuilder<PersonPositionJobBuilder<TSelf>> where TSelf : PersonPositionJobBuilder<TSelf>
{
    /// <summary>
    ///     Imposta la posizione lavorativa della persona e ritorna l'istanza del builder per permettere il method chaining.
    /// </summary>
    public TSelf WorksAsA(string position)
    {
        Person.Position = position;
        return (TSelf)this;
    }
}

/// <summary>
///     Classe figlia di seconda generazione, quindi figlia di JobBuilder il quale è figlio di PersonInfoBuilder
/// </summary>
public class PersonBirthDateBuilder<TSelf> : PersonPositionJobBuilder<PersonBirthDateBuilder<TSelf>> where TSelf : PersonBirthDateBuilder<TSelf>
{
    /// <summary>
    ///     Imposta la data di nascita della persona e ritorna l'istanza del builder per permettere il method chaining.
    /// </summary>
    public TSelf Born(DateTime dateOfBirth)
    {
        Person.DateOfBirth = dateOfBirth;
        return (TSelf)this;
    }
}