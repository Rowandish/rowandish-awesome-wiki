﻿using System;
using DesignPatternExamples.Non_Standard;

namespace DesignPatternExamples.Creationals.Builders;

public enum CarType
{
    Sedan,
    Crossover
}

public class Car
{
    public CarType Type;
    public int WheelSize;

    public override string ToString()
    {
        return $"Car of type: {Type} and Wheel size: {WheelSize}";
    }
}

public interface ISpecifyCarType
{
    public ISpecifyWheelSize OfType(CarType type);
}

public interface ISpecifyWheelSize
{
    public IBuildCar WithWheels(int size);
}

public interface IBuildCar
{
    public Car Build();
}

/// <summary>
///     L'obiettivo di questo metodo builder è forzare l'ordine in cui vengono chiamati i metodi per costruire l'oggetto
///     (come nel FluentInterface, tipo <see cref="FluentBlobTransfer" />) ma applicato alla costruzione di un oggetto
/// </summary>
public static class StepwiseCarBuilder
{
    /// <summary>
    ///     Primo e unico metodo public che fornisce un oggetto dell'interfaccia <see cref="ISpecifyCarType"/>, la quale ha un solo metodo, <see cref="ISpecifyCarType.OfType"/>.
    /// </summary>
    public static ISpecifyCarType Create()
    {
        return new Impl();
    }

    /// <summary>
    ///     Classe privata che fornisce la costruzione dell'oggetto step by step. E' privata così ho la certezza che il mondo esterno non la veda dato che non serve a nessuno
    /// </summary>
    private class Impl :
        ISpecifyCarType,
        ISpecifyWheelSize,
        IBuildCar
    {
        private readonly Car car = new();

        /// <summary>
        ///     Implementazione effettiva dei metodi delle interfacce
        /// </summary>
        public Car Build()
        {
            return car;
        }

        /// <summary>
        ///     Implementazione effettiva dei metodi delle interfacce
        /// </summary>
        public ISpecifyWheelSize OfType(CarType type)
        {
            car.Type = type;
            return this;
        }

        /// <summary>
        ///     Implementazione effettiva dei metodi delle interfacce
        /// </summary>
        public IBuildCar WithWheels(int size)
        {
            switch (car.Type)
            {
                case CarType.Crossover when size is < 17 or > 20:
                case CarType.Sedan when size is < 15 or > 17:
                    throw new ArgumentException($"Wrong size of wheel for {car.Type}.");
            }

            car.WheelSize = size;
            return this;
        }
    }
}