﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignPatternExamples.Creationals.Builders
{
    /// <summary>
    ///     Classe astratta che rappresenta un builder funzionale per la creazione di oggetti TSubject.
    ///     Questo builder consente di definire una serie di azioni da eseguire sull'oggetto durante la sua costruzione.
    /// </summary>
    /// <typeparam name="TSubject">Il tipo di oggetto che si vuole costruire</typeparam>
    /// <typeparam name="TSelf">Il tipo di questo builder stesso, per supportare l'ereditarietà ricorsiva</typeparam>
    public abstract class FunctionalBuilder<TSubject, TSelf>
        where TSelf : FunctionalBuilder<TSubject, TSelf>
        where TSubject : new()
    {
        /// <summary>
        ///     Lista delle azioni da eseguire sull'oggetto durante la costruzione
        /// </summary>
        private readonly List<Func<TSubject, TSubject>> _actions = new();

        /// <summary>
        ///     Aggiunge un'azione da eseguire sull'oggetto durante la costruzione.
        /// </summary>
        /// <param name="action">L'azione da eseguire sull'oggetto</param>
        /// <returns>Il builder stesso, per consentire la concatenazione dei metodi</returns>
        public TSelf Do(Action<TSubject> action) => AddAction(action);

        /// <summary>
        ///     Aggiungere un'azione alla lista delle cose da fare
        /// </summary>
        private TSelf AddAction(Action<TSubject> action)
        {
            _actions.Add(self => {
                action(self);
                return self;
            });
            return (TSelf) this;
        }

        /// <summary>
        ///     Costruisce l'oggetto TSubject applicando tutte le azioni definite.
        ///     Ill metodo Aggregate() combina tutti gli elementi della lista utilizzando una funzione specificata.
        ///     In questo caso, si inizia con un oggetto new TSubject() come valore iniziale e per ogni funzione f
        ///     presente nella lista _actions, viene applicata a subject, che rappresenta l'oggetto in costruzione, restituendo l'oggetto modificato.
        /// </summary>
        /// <returns>L'oggetto TSubject costruito</returns>
        public TSubject Build() => _actions.Aggregate(new TSubject(), (subject, f) => f(subject));
    }

    public sealed class FunctionalPersonBuilder : FunctionalBuilder<Person, FunctionalPersonBuilder>
    {
        public FunctionalPersonBuilder Called(string name) => Do(p => p.Name = name);
        public FunctionalPersonBuilder WorksAsA(string position) => Do(p => p.Position = position);
    }
}