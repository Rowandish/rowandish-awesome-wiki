﻿namespace DesignPatternExamples.Creationals.Builders
{
  /// <summary>
    /// Rappresenta una persona con dettagli relativi all'indirizzo e all'impiego.
    /// </summary>
    public class PersonWithAddress
    {
        /// <summary>
        /// Indirizzo della persona.
        /// </summary>
        public string StreetAddress, Postcode, City;

        /// <summary>
        /// Impiego della persona.
        /// </summary>
        public string CompanyName, Position;

        /// <summary>
        /// Reddito annuo della persona.
        /// </summary>
        public int AnnualIncome;

        /// <summary>
        /// Restituisce una rappresentazione testuale dell'oggetto PersonWithAddress.
        /// </summary>
        /// <returns>Una stringa che rappresenta l'oggetto PersonWithAddress.</returns>
        public override string ToString()
        {
            return $"{nameof(StreetAddress)}: {StreetAddress}, {nameof(Postcode)}: {Postcode}, {nameof(City)}: {City}, {nameof(CompanyName)}: {CompanyName}, {nameof(Position)}: {Position}, {nameof(AnnualIncome)}: {AnnualIncome}";
        }
    }

    /// <summary>
    ///     Facade per creare un oggetto PersonWithAddress.
    /// </summary>
    public class FacetedPersonBuilder
    {
        /// <summary>
        ///     L'oggetto che stiamo per costruire, passato come riferimento (non struct)
        /// </summary>
        protected PersonWithAddress PersonWithAddress = new();

        /// <summary>
        /// Permette di iniziare a costruire l'indirizzo di una persona.
        /// </summary>
        public PersonAddressBuilder Lives => new(PersonWithAddress);

        /// <summary>
        /// Permette di iniziare a costruire il lavoro di una persona.
        /// </summary>
        public PersonJobBuilder Works => new(PersonWithAddress);

        /// <summary>
        ///     Converte implicitamente un oggetto PersonBuilder in un oggetto PersonWithAddress se viene chiesto esplicitamente dall'esterno (esempio utilizzando PersonWithAddress al posto di var)
        /// </summary>
        /// <param name="pb">Il PersonBuilder da convertire.</param>
        /// <returns>L'oggetto PersonWithAddress risultante.</returns>
        public static implicit operator PersonWithAddress(FacetedPersonBuilder pb)
        {
            return pb.PersonWithAddress;
        }
    }

    /// <summary>
    /// Builder per aggiungere dettagli relativi al lavoro di una persona.
    /// </summary>
    public class PersonJobBuilder : FacetedPersonBuilder
    {
        /// <summary>
        /// Inizializza una nuova istanza di PersonJobBuilder.
        /// </summary>
        /// <param name="personWithAddress">La persona a cui aggiungere dettagli relativi al lavoro.</param>
        public PersonJobBuilder(PersonWithAddress personWithAddress)
        {
            PersonWithAddress = personWithAddress;
        }

        public PersonJobBuilder At(string companyName)
        {
            PersonWithAddress.CompanyName = companyName;
            return this;
        }

        public PersonJobBuilder AsA(string position)
        {
            PersonWithAddress.Position = position;
            return this;
        }

        /// <summary>
        /// Imposta il reddito annuo della persona.
        /// </summary>
        /// <param name="annualIncome">Il reddito annuo della persona.</param>
        /// <returns>Il builder stesso.</returns>
        public PersonJobBuilder Earning(int annualIncome)
        {
            PersonWithAddress.AnnualIncome = annualIncome;
            return this;
        }
    }

    /// <summary>
    /// Builder per aggiungere dettagli relativi all'indirizzo di una persona.
    /// </summary>
    public class PersonAddressBuilder : FacetedPersonBuilder
    {
        /// <summary>
        /// Inizializza una nuova istanza di PersonAddressBuilder.
        /// </summary>
        /// <param name="personWithAddress">La persona a cui aggiungere dettagli relativi all'indirizzo.</param>
        public PersonAddressBuilder(PersonWithAddress personWithAddress)
        {
            PersonWithAddress = personWithAddress;
        }

        public PersonAddressBuilder At(string streetAddress)
        {
            PersonWithAddress.StreetAddress = streetAddress;
            return this;
        }

        public PersonAddressBuilder WithPostcode(string postcode)
        {
            PersonWithAddress.Postcode = postcode;
            return this;
        }

        public PersonAddressBuilder In(string city)
        {
            PersonWithAddress.City = city;
            return this;
        }
    }
}
