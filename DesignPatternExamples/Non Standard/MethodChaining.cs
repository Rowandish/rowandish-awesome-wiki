﻿using System.Collections.Generic;
using System.Linq;

namespace DesignPatternExamples.Non_Standard
{
    /// <summary>
    ///     Il method chaining è un metodo per ottenere un oggetto complesso tramite la chiamata a vari metodi interscambiabili
    ///     tra di loro e con un linguaggio alla LINQ.
    ///     In particolare si basa tutto sul ritorno di this alla fine di ogni metodo concatenabile, ogni metodo inoltre
    ///     modifica lo stato interno della classe.
    ///     Esiste spesso un metodo terminatore che invece di ritornare this, fornisce una variabile interna alla classe appena
    ///     costruita.
    /// </summary>
    public class MethodChaining
    {
        /// <summary>
        ///     Variabile di stato interna alla classe da modificare
        /// </summary>
        private List<string> _listWords;

        /// <summary>
        ///     Costruttore
        /// </summary>
        public MethodChaining(List<string> listWords)
        {
            _listWords = listWords;
        }

        /// <summary>
        ///     Terminatore della catena: non ritorna this ma ritorna la variabile di stato costruita. Se non serve ritornare nulla
        ///     questo metodo può essere void (per esempio un metodo di Init())
        /// </summary>
        public List<string> GetWords()
        {
            return _listWords;
        }

        /// <summary>
        ///     Metodo che agisce sullo stato interno del sistema  (modifica <see cref="_listWords" />) e ritorna this per la
        ///     concatenazione
        /// </summary>
        public MethodChaining FilterOutShortWords()
        {
            _listWords = _listWords.Where(w => w.Length >= 3).ToList();
            return this;
        }
        /// <summary>
        ///     Metodo che agisce sullo stato interno del sistema  (modifica <see cref="_listWords" />) e ritorna this per la
        ///     concatenazione
        /// </summary>
        public MethodChaining FilterOnlyWordsContainingLetterC()
        {
            _listWords = _listWords.Where(word => word.ToLower().Contains("c")).ToList();
            return this;
        }
        /// <summary>
        ///     Metodo che agisce sullo stato interno del sistema  (modifica <see cref="_listWords" />) e ritorna this per la
        ///     concatenazione
        /// </summary>
        public MethodChaining FilterArbitraryComplexItems()
        {
            /* ... some arbitrarily complicated code ... */
            return this;
        }
        /// <summary>
        ///     Metodo che agisce sullo stato interno del sistema  (modifica <see cref="_listWords" />) e ritorna this per la
        ///     concatenazione
        /// </summary>
        public MethodChaining FilterSomeMoreArbitraryComplexItems()
        {
            /* ... some arbitrarily complicated code ... */
            return this;
        }
    }
}
