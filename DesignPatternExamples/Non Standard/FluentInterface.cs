﻿using System.IO;

namespace DesignPatternExamples.Non_Standard
{
    /// <summary>
    ///     FluentInterface: questo pattern è una versione più complessa del method chaining; la differenza è che ogni metodo
    ///     ritorna una interface diversa, questo per garantire flessibilità e estendibilità. Inoltre posso evitare il loop
    ///     infinito dei metodi inquanto posso obbligare il concatenamento solo in un determinato ordine.
    ///     Inoltre le operazioni effettive vengono eseguite solo alla fine, il chaining è usato solo per inizializzare
    ///     strutture dati interne.
    ///     In questo esempio voglio creare una classe che effettua il download a upload di un file su pc locale o su Azure,
    ///     eventualmente asincrono.
    ///     Per prima cosa è necessario pensare alla chiamata ai metodi in linguaggio naturale pensando a tutte le combinazioni
    ///     ammesse e non ammesse.
    ///     FluentBlobTransfer.Connect(..).OnBlob(..).Download(..).ToFile(..);
    ///     FluentBlobTransfer.Connect(..).OnBlob(..).Download(..).ToStream(..);
    ///     FluentBlobTransfer.Connect(..).OnBlob(..).Upload(..).FromFile(..);
    ///     FluentBlobTransfer.Connect(..).OnBlob(..).Upload(..).FromStream(..);
    ///     In secondo logo esplicitiamo le combinazioni ammesse e non ammesse. In questo caso Download From File/Stream e
    ///     Upload To File/Stream sono combinazioni non ammesse.
    ///     Per ottenere questo il metodo Download deve ritornare una interface con solo i metodi ToFile() e ToStream() mentre
    ///     Upload solo con i From.
    ///     Una volta definito bene il caso d'uso è necessario definire le interfacce richieste, seguendo le seguenti regole:
    ///     * Il primo e l'ultimo metodo della catena possono essere senza parametri
    ///     * Ogni metodo deve avere massimo un argomento
    ///     * Usa metodi con nomi parlanti
    ///     * Copri tutti i casi possibili
    /// </summary>
    /// <summary>
    ///     Questa interfaccia serve per obbligare il chaining solo del metodo OnBlob, quindi sarà utilizzata dal metodo
    ///     <see cref="FluentBlobTransfer.Connect" />.
    ///     In questo modo obbligo a chiamare sempre OnBlob dopo il Connect
    /// </summary>
    internal interface IAzureBlob
    {
        IAzureAction OnBlob(string blobBlockPath);
    }

    /// <summary>
    ///     Questa interfaccia serve per obbligare il chaining solo dei metodi Download e Upload, quindi sarà utilizzata dal
    ///     metodo <see cref="FluentBlobTransfer.OnBlob" />.
    ///     Dato che i metodi ritornano una istanza di <see cref="IAzureWrite" /> e <see cref="IAzureRead" /> posso
    ///     concatenarla sia con ToFile/Stream che FromFile/Stream
    /// </summary>
    internal interface IAzureAction
    {
        IAzureWrite Download(string fileName);
        IAzureRead Upload(string fileName);
    }

    /// <summary>
    ///     Questa interfaccia serve per obbligare il chaining solo dei metodi ToFile e ToStream, quindi sarà utilizzata dal
    ///     metodo <see cref="FluentBlobTransfer.Download" />
    /// </summary>
    internal interface IAzureWrite
    {
        void ToFile(string filePath);
        void ToStream(Stream stream);
    }

    /// <summary>
    ///     Questa interfaccia serve per obbligare il chaining solo dei metodi FromFile e FromStream, quindi sarà utilizzata
    ///     dal metodo <see cref="FluentBlobTransfer.Upload" />
    /// </summary>
    internal interface IAzureRead
    {
        void FromFile(string filePath);
        void FromStream(Stream stream);
    }

    /// <summary>
    ///     Questa è la classe principale che sto costruendo, deve avere le seguenti caratteristiche:
    ///     * Eredita da tutte le interfacce di cui sopra, questo per poter avere il chaining (punto fondamentale)
    ///     * Il costruttore deve essere privato, tutto deve essere inizializzato con il chaining
    ///     * Nessuno deve ereditarci, la esplicito sealed
    ///     * I nomi dei metodi e degli argomenti deve essere parlante
    ///     * Al massimo un parametro in ingresso per ogni metodo
    /// </summary>
    internal sealed class FluentBlobTransfer : IAzureBlob, IAzureAction, IAzureWrite, IAzureRead
    {
        /// <summary>
        /// Variabili di stato interne al sistema
        /// </summary>
        private readonly string _connectionString;
        private string _blobBlockPath;
        private string _fileName;

        /// <summary>
        ///     Costruttore privato, lo chiamo dal'entry point statico
        /// </summary>
        private FluentBlobTransfer(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        ///     Questi metodi servono solo per la configurazione dello stato interno del sistema ma non fanno nulla, tutte le
        ///     operazioni verranno fatte dal metodo finale.
        /// </summary>
        public IAzureWrite Download(string fileName)
        {
            _fileName = fileName;

            return this;
        }

        public IAzureRead Upload(string fileName)
        {
            _fileName = fileName;

            return this;
        }

        public IAzureAction OnBlob(string blobBlockPath)
        {
            _blobBlockPath = blobBlockPath;

            return this;
        }

        /// <summary>
        ///     I metodi finali ritornano void per indicare che sono i metodi conclusivi della catena. Qualora (e non è questo il
        ///     caso) il metodo finale ritorni un oggetto deve avere un nome parlante che esplicita l'azione, per esempio Execute()
        ///     o GetResult().
        /// </summary>
        public void FromFile(string filePath)
        {
            // Code to upload from file to Azure Blob Storage
        }

        public void FromStream(Stream stream)
        {
            // Code to upload from stream to Azure Blob Storage
        }

        public void ToFile(string filePath)
        {
            // Code to download from Azure Blob Storage to file
        }

        public void ToStream(Stream stream)
        {
            // Code to download from Azure Blob Storage to stream
        }

        /// <summary>
        ///     Il metodo di ingresso deve essere statico, deve essere un verbo per esplicitare l'intento. Questo metodo
        ///     tipicamente chiama il costruttore della classe stessa
        /// </summary>
        public static IAzureBlob Connect(string connectionString)
        {
            return new FluentBlobTransfer(connectionString);
        }
    }
}