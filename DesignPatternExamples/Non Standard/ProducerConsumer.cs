﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace DesignPatternExamples.Non_Standard
{
    internal class ProducerConsumer
    {
        /// <summary>
        ///     Insieme che contiene i dati prodotti dal produttore e presi dal consumatore
        /// </summary>
        private BlockingCollection<int> _blockingCollection;

        /// <summary>
        ///     Il produttore crea dei numero interi da 0 a 9 e impiega 1 secondo tra un numero e l'altro
        /// </summary>
        private async Task StartProducer()
        {
            await Task.Run(async () =>
            {
                for (var i = 0; i < 5; i++)
                {
                    _blockingCollection.Add(i);
                    Console.WriteLine($"Producing: {i}");
                    // Attesa di un secondo tra un ciclo e l'altro
                    await Task.Delay(1000);
                }

                // Need to do this to keep foreach below from hanging
                _blockingCollection.CompleteAdding();
            });
        }

        /// <summary>
        ///     Il consumatore, quando un numero è stato prodotto da produttore, lo printa a schermo. Il metodo
        ///     GetConsumingEnumerable() è un metodo bloccante che aspetta che venga prodotto almeno un dato (che di fatto è la
        ///     chiamata al metodo Add o AddRange della BlockingCollection).
        ///     Per uscire dal forwach è necessario che il produttore chiami il metodo CompleteAdding().
        /// </summary>
        private async void StartConsumer()
        {
            foreach (var item in _blockingCollection.GetConsumingEnumerable())
                Console.WriteLine($"Consuming: {item}");
        }

        public async Task StartProducerConsumerCycle()
        {
            _blockingCollection = new BlockingCollection<int>();
            // Faccio partire il producer asincronto, ma non lo awaito subito ma dopo aver lanciato il consumer
            var t = StartProducer();
            StartConsumer();
            // Aspetto che il task sopra sia completato prima di disposare la collection
            await t;
            _blockingCollection.Dispose();
        }
    }
}