﻿using System;
using System.Collections.Generic;

namespace DesignPatternExamples.Non_Standard.Visitor
{
    /// <summary>
    /// Ho una generica interfaccia shape: 
    /// </summary>
    public interface IShapeBad
    {
        void Draw();
        void Fill();
    }

    public class CircleBad : IShapeBad
    {
        public void Draw()
        {
            Console.WriteLine("Circle: Draw");
        }

        public void Fill()
        {
            Console.WriteLine("Circle: Fill");
        }
    }

    public class SquareBad : IShapeBad
    {
        public void Draw()
        {
            Console.WriteLine("Square: Draw");
        }

        public void Fill()
        {
            Console.WriteLine("Square: Fill");
        }
    }

    public static class Drawer
    {
        public static void DrawShapes(IEnumerable<IShapeBad> shapes)
        {
            foreach (var shape in shapes) shape.Draw();
        }
    }
}