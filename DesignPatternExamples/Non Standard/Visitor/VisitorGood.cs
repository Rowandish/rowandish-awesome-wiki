﻿using System;
using System.Collections.Generic;

namespace DesignPatternExamples.Non_Standard.Visitor
{
    public interface IShape
    {
        void Accept(IVisitor visitor);
    }

    public class Circle : IShape
    {
        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class Square : IShape
    {
        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    //
    // Visitors algorithms on shapes elements
    // don't use the IAlgorithm terminology to keep up with the classical visitor pattern terminology
    //
    public interface IVisitor
    {
        void Visit(Circle circle);
        void Visit(Square square);
    }

    public class DrawAlgorithm : IVisitor
    {
        public void Visit(Circle circle)
        {
            Console.WriteLine("Circle: Draw");
        }

        public void Visit(Square square)
        {
            Console.WriteLine("Square: Draw");

        }
    }

    public class FillAlgorithm : IVisitor
    {
        public void Visit(Circle circle)
        {
            Console.WriteLine("Circle: Fill");

        }

        public void Visit(Square square)
        {
            Console.WriteLine("Square: Fill");
        }
    }

    public static class Program
    {
        public static void ApplyVisitorAlgorithmOnShapesElements(IEnumerable<IShape> shapes, IVisitor visitor)
        {
            foreach (var shape in shapes)
                // Double dispatching:
                //   shape can be both: Circle or Square
                //   visitor can be both Draw or Persist
                shape.Accept(visitor);
        }
    }
}