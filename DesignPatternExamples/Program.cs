﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DesignPatternExamples.Behavioural.State;
using DesignPatternExamples.Comportamentali;
using DesignPatternExamples.Creationals.Builders;
using DesignPatternExamples.Creationals.Factories;
using DesignPatternExamples.Creationals.Prototipes;
using DesignPatternExamples.Creationals.Singletons;
using DesignPatternExamples.Non_Standard;
using DesignPatternExamples.Strutturali;
using DesignPatternExamples.Strutturali.Composite;
using Person = DesignPatternExamples.Creationals.Builders.Person;

namespace DesignPatternExamples;

internal class Program
{
    public static async Task Main(string[] args)
    {
        Console.WriteLine("-------- Non Standard --------");
        Console.WriteLine("--- Method chaining ---");

        var listWords = new List<string>(new[]
            { "this", "is", "a", "sample", "list", "of", "words", "that", "demonstrates", "some", "code" });

        var filteredWords = new MethodChaining(listWords)
            .FilterOutShortWords()
            .FilterOnlyWordsContainingLetterC()
            .FilterArbitraryComplexItems()
            .FilterSomeMoreArbitraryComplexItems()
            .GetWords();

        Console.WriteLine(string.Join(", ", filteredWords.ToArray()));

        Console.WriteLine("--- Fluent Interface ---");
        FluentBlobTransfer
            .Connect("storageAccountConnectionString")
            .OnBlob("blobName")
            .Download("fileName")
            .ToFile(@"D:\Azure\Downloads\");

        Console.WriteLine("-------- Strutturali --------");
        Structurals();

        Console.WriteLine("-------- Creazionali --------");

        await Factories();
        Builders();
        Prototipes();
        Singletons();


        Console.WriteLine("-------- Behaviourals --------");

        // Lo State è un pattern in cui il comportamento di un oggetto è determinato dal suo stato e l'oggetto gestisce il passaggio da uno stato all'altro. L'insieme degli stati e delle transizioni è detta macchina a stati finiti
        Console.WriteLine("--- State ---");
        // Inizializzo il Context con lo stato concreto ConcreteStateA
        var c = new Context(new ConcreteClassicStateA());
        // Lancio varie richieste, ognuna di queste modifica lo stato interno del Context
        c.Request();
        c.Request();
        c.Request();
        c.Request();

        var phone = new DictionaryState();
        phone.PrintAvailableTriggers();
        phone.MoveNext(DictionaryState.Trigger.CallDialed);
        phone.PrintAvailableTriggers();
        phone.MoveNext(DictionaryState.Trigger.CallConnected);
        phone.PrintAvailableTriggers();

        Console.WriteLine("--- Template ---");

        new ConcreteClass1().TemplateMethod();
        new ConcreteClass2().TemplateMethod();

        Console.WriteLine("--- Producer Consumer ---");
        var producerConsumer = new ProducerConsumer().StartProducerConsumerCycle();

        Console.ReadKey();
    }

    private static void Structurals()
    {
        Console.WriteLine("--- Decorator ---");
        // L'obiettivo del decorator è aggiungere funzionalità ad un oggetto senza modificarlo (Open closed principle) e
        // mantenendo le nuove funzionalità separate dal codice originale (Single responsability)
        // Tipicamente l'oggetto decorator mantiene un riferimento all'oggetto da decorare
        IElement[] vars =
        {
            new ElementWithXDecoration(new CoreElement()),
            new ElementWithYDecoration(new ElementWithXDecoration(new CoreElement())),
            new ElementWithZDecoration(new ElementWithYDecoration(new ElementWithXDecoration(new CoreElement())))
        };
        foreach (var item in vars)
        {
            Console.WriteLine(item.GetIngredients());
            Console.WriteLine(item.GetPrice());
        }

        Console.WriteLine("--- Adapter ---");
        // In generale l'adapter permette di creare una sorta di wrapper tra delle API che ho ora e delle API che vorrei.
        // Tipicamente si crea un componente che ha una reference alla classe che voglio wrappare che espone le API che voglio.

        // L'obiettivo è creare una classe che faccia da "Adapter" al concetto di Vector, quindi fornisce un array di dimensione definita di un tipo definito
        // Voglio creare classi figlie di <T, numero> anche se in c# non posso mettere un numero tra i tipi, lo faccio con un trucco.
        // La factory inoltre ritorna il tipo di dato del chiamante, per esempio in questo caso il tipo di dato di "v" è "Vector2I"
        var v = Vector2I.Create(1, 2);
        v[0] = 0;

        var vv = Vector2I.Create(3, 2);
        // Posso sovrascrivere l'operazione "+" tra i miei vector
        var result = v + vv;

        // La dichiarazione sotto non posso farla in quanto dovrei copincollare la dichiarazione del costruttore in tutti i figli. Risolvo con la factory
        //var vv3 = new Vector3F(3.5f, 2.2f, 1);
        // il tipo di dato di "istance" (sotto esplicitato) è Vector3F: questa cosa la ottengo in quanto la factory ritorna TSelf
        Vector3F istance = Vector3F.Create(3.5f, 2.2f, 1);
        istance.CustomMethodOfVector3F();

        Console.WriteLine("--- Bridge ---");
        // Lo scopo di questo pattern è connettere dei componenti tramite delle astrazioni (interfacce o classi astratte)
        // al fine di evitare l'esplosione di classi tramite il "prodotto cartesiano". Esempio classico la classe X che può andare su Windows o Linux
        // e può avere il comportamento A o B. Non voglio creare WindowsXComportamentoA, WindowsXComportamentoB e così via...
        // Si risolve invece di usare sempre l'ereditarietà usare l'HAS, stile strategy.
        var circleVector = new Circle(new VectorRenderer(), 5);
        circleVector.Draw();
        circleVector.Resize(2);
        circleVector.Draw();
        var circleRaster = new Circle(new RasterRenderer(), 5);
        circleRaster.Draw();
        circleRaster.Resize(2);
        circleRaster.Draw();

        Console.WriteLine("--- Composite ---");
        // Lo scopo di questo pattern è trattare allo stesso modo oggetti singoli (scalari) di collection di tali oggetti.
        // Per esempio avere che Foo e Collection<Foo> possano fornire le stesse API
        // In questo caso ho l'oggetto "My Drawing"
        var drawing = new GraphicObject {Name = "My Drawing"};
        drawing.Children.Add(new SquareGraphicObject {Color = "Red"});
        drawing.Children.Add(new CircleGraphicObject{Color="Yellow"});

        var group = new GraphicObject();
        group.Children.Add(new CircleGraphicObject{Color="Blue"});
        group.Children.Add(new SquareGraphicObject{Color="Blue"});
        drawing.Children.Add(group);

        Console.WriteLine(drawing);

        Console.WriteLine("--- Composite with yield return ---");
        var neuron1 = new Neuron();
        var neuron2 = new Neuron();
        var layer1 = new NeuronLayer();
        var layer2 = new NeuronLayer();

        neuron1.ConnectTo(neuron2);
        neuron1.ConnectTo(layer1);
        layer1.ConnectTo(layer2);
    }

    /// <summary>
    /// Con il singleton voglio creare una classe la cui istanza sia la stessa o per tutto il codice o eventualmente sul singolo thread.
    /// Questo pattern è quasi sempre code-smell e andrebbe usato il meno possibile.
    /// </summary>
    private static void Singletons()
    {
        Console.WriteLine("--- Singleton ---");

        // L'obiettivo è creare una classe che abbia una sola istanza in tutto il codice
        Console.WriteLine("- Classic Singleton -");
        var obj1 = SingletonObject.Instance;
        var obj2 = SingletonObject.Instance;
        Console.WriteLine($"Is obj1 == obj2?: {obj1 == obj2}");

        Console.WriteLine("- Per Thread Singleton -");
        // L'obiettivo è avere un singleton per ogni thread invece che avere un singleton generale per tutta l'app
        // Questa cosa si può fare anche con la DI esplicitando lo scope dell'oggetto
        var t1 = Task.Factory.StartNew(() =>
        {
            Console.WriteLine("t1: " + PerThreadSingleton.Instance.Id);
        });
        var t2 = Task.Factory.StartNew(() =>
        {
            Console.WriteLine("t2: " + PerThreadSingleton.Instance.Id);
            Console.WriteLine("t2 again: " + PerThreadSingleton.Instance.Id);
        });
        Task.WaitAll(t1, t2);

        Console.WriteLine("- Ambient Context -");
        // L'obiettivo è creare delle variabili di ambiente che posso modificare in un determinato scope utilizzando lo using

        Console.WriteLine($"Initial Variable value = {AmbientContext.Current.AmbientVariableToBeChanged}");
        using (new AmbientContext(1234))
        {
            Console.WriteLine($"Nested variable value = {AmbientContext.Current.AmbientVariableToBeChanged}");
            using (new AmbientContext(4567))
                Console.WriteLine($"Nested 2nd level variable value = {AmbientContext.Current.AmbientVariableToBeChanged}");
        }
        Console.WriteLine($"Initial Variable value = {AmbientContext.Current.AmbientVariableToBeChanged}");
    }

    /// <summary>
    /// Il pattern prototype permette di costruire oggetti clonandoni altri invece di partire da 0.
    /// Inoltre può essere utile per esplicitare l'utilizzo di una DeepCopy o ShallowCopy invece di usare il metodo Clone() che non è parlante
    /// </summary>
    private static void Prototipes()
    {
        Console.WriteLine("--- Prototype ---");
        // L'obiettivo è creare oggetti a partire da oggetti prototipi base invece che partire da 0
        var prototypeFactory = new PrototypeFactory();
        var foo = prototypeFactory.CreatePrototype(RecordType.Foo);
        Console.WriteLine($"Creo il prototype Foo: {foo}");
        foo = prototypeFactory.CreatePrototype(RecordType.Foo);
        foo.Name = "NewName";
        Console.WriteLine($"A partire dal clone dello standard ci cambio nome (e guid): {foo}");
        Console.WriteLine($"Creo il prototype Bar: {prototypeFactory.CreatePrototype(RecordType.Bar)}");
        Console.WriteLine(
            $"Creo un secondo prototype Bar clonando il primo: {prototypeFactory.CreatePrototype(RecordType.Bar)}");

        Console.WriteLine("--- Prototype for Deep Copy---");
        // L'obiettivo è evitare di usare il metodo Clone() di .NET che non specifica se è una shallow copy o deep copy e delegare questo comportamento all'interno della classe
        var john = new Employee
        {
            Names = new[] {"John", "Doe"},
            Address = new Address {HouseNumber = 123, StreetName = "London Road"},
            Salary = 321000
        };
        var johnCopy = DeepCopyExtensions.DeepCopy(john);

        johnCopy.Names[1] = "Smith";
        johnCopy.Address.HouseNumber++;
        johnCopy.Salary = 123000;

        Console.WriteLine(john);
        Console.WriteLine(johnCopy);

        Console.WriteLine("--- Deep Copy Through Serialization---");
        // L'obiettivo è sfruttare la serializzazione per fare la Deep Copy di un oggetto in modo "magico" scrivendo molto meno codice dell'esempio sopra
        var objectToBeDeepCloned = new ObjectToBeDeepCloned {Whatever = "abc"};
        var deepCloned = objectToBeDeepCloned.DeepCopy();

        deepCloned.Whatever = "xyz";
        Console.WriteLine(objectToBeDeepCloned);
        Console.WriteLine(deepCloned);
    }

    /// <summary>
    ///     Chiamata di tutti i metodi Factory. Questo pattern è da utilizzare per rendere più parlanti i costruttori, evitare
    ///     costruttori con mille parametri e gestire oggetti che hanno comunque una creazione semplice e non a pezzi
    /// </summary>
    private static async Task Factories()
    {
        Console.WriteLine("--- Factory ---");

        Console.WriteLine("- Simple avoid constructor calls -");
        // L'obiettivo è avere costruttori che fanno cose diverse ma con gli stessi parametri in ingresso
        var cartesianPoint = Point.PointFactory.CreateFromCartesianCoordinates(3, 4);
        Console.WriteLine($"Point with Cartesian coordinates: ({cartesianPoint.X}, {cartesianPoint.Y})");
        var polarPoint =
            Point.PointFactory.CreateFromPolarCoordinates(5, Math.PI / 4); // Radius = 5, Angle = 45° in radians
        Console.WriteLine($"Point with polar coordinates: ({polarPoint.X}, {polarPoint.Y})");

        Console.WriteLine("- Async Factory -");
        // L'obiettivo è creare un oggetto con l'await, cosa impossibile in un costruttore normale
        var user = await AsyncFactoryUser.CreateUserAsync("exampleUser");
        Console.WriteLine($"User '{user.Username}' authenticated: {user.IsAuthenticated}");

        Console.WriteLine("- Bulk replacement factory -");
        // L'obiettivo è avere un modo per sostituire tutti gli oggetti forniti da una factory in una volta sola
        var replaceableThemeFactory = new ReplaceableThemeFactory();
        var magicTheme = replaceableThemeFactory.CreateTheme(true);
        Console.WriteLine(magicTheme.Value.BgrColor); // dark gray
        replaceableThemeFactory.ReplaceTheme(false);
        Console.WriteLine(magicTheme.Value.BgrColor); // white

        // L'obiettivo è fornire alla classe Client la factory per creare gli oggetti che vuole tramite interfaccia
        Console.WriteLine("- Abstract Factory -");
        var concreteFactory1 = new ConcreteFactory1();
        var client1 = new Client(concreteFactory1);
        Console.WriteLine(client1.Run());
        var concreteFactory2 = new ConcreteFactory2();
        var client2 = new Client(concreteFactory2);
        Console.WriteLine(client2.Run());
        var client3 = new Client(concreteFactory1.CreateProductA(), concreteFactory2.CreateProductB());
        Console.WriteLine(client3.Run());
    }

    /// <summary>
    /// I pattern builder sono da utilizzare tipicamente quando la costruzione di un oggetto avviene in più parti distinte e non in una volta sola come nel factory
    /// </summary>
    private static void Builders()
    {
        Console.WriteLine("--- Builders ---");

        Console.WriteLine("- Standard Builder -");

        // Faccio sia l'esempio con il builder classico che con quello fluent
        var director = new Director(new ConcreteBuilder());
        var fluentDirector = new FluentDirector(new FluentConcreteBuilder());
        Console.WriteLine("Product minimal with standard builder:");
        director.BuildMinimalViableProduct();
        var product = fluentDirector.BuildMinimalViableProduct();
        Console.WriteLine(director.GetProduct().ListParts());
        Console.WriteLine(product.ListParts());

        Console.WriteLine("Product full optional with standard builder:");
        director.BuildFullFeaturedProduct();
        var fullProduct = fluentDirector.BuildFullFeaturedProduct();
        Console.WriteLine(director.GetProduct().ListParts());
        Console.WriteLine(fullProduct.ListParts());


        director.Builder = new CustomBuilder();
        Console.WriteLine("Product minimal with custom builder:");
        director.BuildMinimalViableProduct();
        Console.WriteLine(director.GetProduct().ListParts());

        Console.WriteLine("Product full optional with custom builder:");
        director.BuildFullFeaturedProduct();
        Console.WriteLine(director.GetProduct().ListParts());

        // Il director è opzionale, posso usare direttamente il builder
        Console.WriteLine("Product without builder:");
        var builder = new ConcreteBuilder();
        builder.BuildPartA();
        builder.BuildPartC();
        Console.WriteLine(builder.GetProduct().ListParts());

        var fluentBuilder = new FluentConcreteBuilder();
        fluentBuilder.BuildPartA().BuildPartC();
        var customProduct = fluentBuilder.GetProduct();
        Console.WriteLine(customProduct.ListParts());


        Console.WriteLine("- Fluent Builder With Recursive Generics -");
        var me = Person.New //Ritorna un generico oggetto Builder
            .Called("Dmitri")
            .WorksAsA("Quant")
            .Born(DateTime.UtcNow)
            .Build();

        Console.WriteLine(me);

        Console.WriteLine("- Stepwise builder with fluent interface -");
        var car = StepwiseCarBuilder.Create()
            .OfType(CarType.Crossover)
            .WithWheels(18)
            .Build();
        Console.WriteLine(car);

        Console.WriteLine("- Functional builder -");
        var pb = new FunctionalPersonBuilder();
        var functionalPerson = pb.Called("Dmitri").WorksAsA("Programmer").Build();
        Console.WriteLine(functionalPerson);

        Console.WriteLine("- Faceted builder -");
        var fpb = new FacetedPersonBuilder();
        PersonWithAddress personWithAddress = fpb
            .Lives
            .At("123 London Road")
            .In("London")
            .WithPostcode("SW12BC")
            .Works
            .At("Fabrikam")
            .AsA("Engineer")
            .Earning(123000);

        Console.WriteLine(personWithAddress);
    }
}