﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace TaskCancellation
{
    /// <summary>
    ///     Applicazione di esempio di come coniugare operazioni lunghe in thread separati con l'interfaccia, senza utilizzare
    ///     Dispatcher e lasciando che questa sia fluida e non freezi mai.
    ///     Inoltre gestisco anche la cancellazione dell'operazione, sempre senza eccezioni o freeze
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     returns the sum of the nth root of all integers from one to 10 million, where n is a parameter.
        /// </summary>
        public double SumRootN(int root)
        {
            double result = 0;
            for (var i = 1; i < 10000000; i++)
            {
                // se qualcuno ha richiesto la cancellazione del token, esco subito lanciando la classica eccezione dei task senza continuare oltre. In questo modo posso uscire da task che erano partiti quando il token era abilitato, ma questo viene disabilitato durante l'esecuzione.
                // VS mostra una eccezione per questo metodo, anche se in verità viene gestita internamente dai vari ContinueWith.
                // Con questa riga da quando premo il pulsante in poi tutti i task risultano cancellati, altrimenti avrei alcuni task cancellati e altri meno.
                _tokenSource.Token.ThrowIfCancellationRequested();
                result += Math.Exp(Math.Log(i) / root);
            }
            return result;
        }

        /// <summary>
        ///     CancellationTokenSource per poter cancellare i thread di calcolo SumRootN
        /// </summary>
        private CancellationTokenSource _tokenSource = new CancellationTokenSource();

        // Utilizzato per evitare il Dispatcher.BeginInvoke sulle operazioni che riguardano il thread dell'interfaccia
        // se viene passato ai metodi ContinueWith o ContinueWhenAll indica che tali operazioni devono essere effettuate nello stesso contesto della variabile ui, quindi il thread dell'interfaccia.
        // Semplicamente passando questo oggetto evito tutti i Dispatcher nelle lambda dei Continue dei Task.
        // N.B. passare questo oggetto significa che le operazioni nella lampbda saranno eseguite nel thread dell'interfaccia, quindi è assolutamente da evitare di effettuare operazioni lunghe che freezzerebbero tutto
        private readonly TaskScheduler _ui = TaskScheduler.FromCurrentSynchronizationContext();

        public void Start_Click(object sender, RoutedEventArgs e)
        {
            TextBlock1.Text = string.Empty;
            var watch = Stopwatch.StartNew();

            // ad ogni pressione di Start creo un nuovo CancellationTokenSource, in quanto una volta che un token è in stato Cancel non ho alcun modo per riportarlo allo stato operativo
            _tokenSource = new CancellationTokenSource();
            var tasks = new List<Task>();

            // Chiamo il metodo SumRootN da 2 a 19 e uso lo Stopwatch per misurare quanto millisecondi ci metto in totale
            for (var i = 2; i < 20; i++)
            {
                // copio il valore di i nella variabile j (closure): i è usato nel loop e quanto il thread comincia a lavorarci tale valore è cambiato (tipicamente sono già uscito dal for quindi sarebbe sempre uguale a 20)
                var j = i;

                // creo un nuovo task che effettua una operazione potenzialmente lunga e ne ritorna il risultato. Il metodo Task.Factory.StartNew ritorna un task, che devo salvare in una variabile
                // passo il Token di _tokenSource in quanto voglio poter cancellare il task
                var outputTask = Task.Factory.StartNew(() => SumRootN(j), _tokenSource.Token);
                tasks.Add(outputTask);

                // ContinueWith viene chiamato quando il metodo creato nel task ritorna un valore. In questo caso scrivo il valore (property Result del Task che mi sero salvato prima) ad interfaccia.
                // Grazie alla property Result posso separare l'operazione lenta SumRootN (non nel thread dell'interfaccia) con l'operazione veloce di modifica testo che invece deve essere eseguita nel thread dell'interfaccia, grazie alla variabile ui
                // Problema: il task potrebbe essere stato cancellato dall'esterno, in tal caso outputTask.Result ritorna una AggregateException -> TaskCanceledException che mi comunica che sto cercando di accedere al valore ritornato da un Task che è stato cancellato, cosa ovviamente non possibile
                // Per evitare questa cosa posso comunicare al metodo ContinueWith di operare solo se il task ritorna un qualche tipo di risultato. Per fare questo utilizzo TaskContinuationOptions.OnlyOnRanToCompletion che permette esattamente questa cosa
                var displayResults = outputTask.ContinueWith(
                    resultTask => TextBlock1.Text += $"root {j} {outputTask.Result}\n", CancellationToken.None,
                    TaskContinuationOptions.OnlyOnRanToCompletion, _ui);

                // Dato che il metodo sopra non viene eseguito se il task viene cancellato, ad interfaccia non ho alcuna informazione su quali task sono stati cancellati.
                // Per eseguire un metodo solo in caso di cancellazione del task posso utilizzare la TaskContinuationOptions con l'opzione TaskContinuationOptions
                var displayCancelledTasks =
                    outputTask.ContinueWith(resultTask => TextBlock1.Text += $"root {j} canceled\n",
                        CancellationToken.None, TaskContinuationOptions.OnlyOnCanceled, _ui);
            }
            // Aspetto che tutti i task nella lista tasks finiscano e poi chiamo il delegate, il quale ferma il timer e printa a schermo il risultato
            Task.Factory.ContinueWhenAll(tasks.ToArray(),
                result =>
                {
                    var time = watch.ElapsedMilliseconds;
                    Label1.Content = $"Milliseconds: {time}";
                }, CancellationToken.None, TaskContinuationOptions.None, _ui);
        }

        /// <summary>
        ///     Pattern produttore consumatore: devo sincronizzare
        /// </summary>
        public void Start2_Click(object sender, RoutedEventArgs e)
        {
            TextBlock1.Text = string.Empty;
            var watch = Stopwatch.StartNew();
            var results = new BlockingCollection<double>();
            var tasks = new List<Task>();
            // Faccio partire il thread del consumatore fuori dal loop. Questo dovrà aspettare che il produttore (SumRootN) calcoli i risultati e li metta nella BlockingCollection. Quando questi vengono aggiunti li mostro a schermo
            var consume = Task.Factory.StartNew(() => Display(results));

            for (var i = 2; i < 20; i++)
            {
                var j = i;

                // Aggiunto alla BlockingCollection il risultato di SumRootN invece di ritornalo come il metodo sopra
                var outputTask = Task.Factory.StartNew(() => { results.Add(SumRootN(j)); });
                tasks.Add(outputTask);
            }
            // Una volta che tutti i task hanno completato le loro operazioni chiamo il metodo CompleteAdding della BlockingCollection che indica che ho finito tale collection non verrà più aggiornata
            // in questo modo gli utilizzatori della collection possono sapere che questa è completa di tutti gli elementi
            Task.Factory.ContinueWhenAll(tasks.ToArray(),
                result =>
                {
                    results.CompleteAdding();
                    var time = watch.ElapsedMilliseconds;
                    Label1.Content = $"Milliseconds: {time}";
                }, CancellationToken.None, TaskContinuationOptions.None, _ui);
            results.Dispose();
        }

        /// <summary>
        ///     Metodo consumatore dei dati prodotti dai task
        /// </summary>
        private void Display(BlockingCollection<double> results)
        {
            // Trucco fondamentale delle BlockingCollection: esco dal foreach solo se la collection è vuota è marcata come Completed (metodo CompleteAdding()), altrimenti rimango bloccato in attesa di nuovi elemeneti
            foreach (var item in results.GetConsumingEnumerable())
            {
                // salvo il currentItem per il motivo spiegato sopra
                var currentItem = item;
                Task.Factory.StartNew(new Action(() => TextBlock1.Text += $"{currentItem}\n"), CancellationToken.None,
                    TaskCreationOptions.None, _ui);
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            _tokenSource.Cancel();
            TextBlock1.Text += "Cancel\n";
        }
    }
}