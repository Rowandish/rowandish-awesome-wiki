﻿namespace SolidProgram.PoliticheAltoLivello;

/// <summary>
///     Politica ad alto livello. A partire da un dizionario string (codice formato) e Lista di string (i codici prodotto associati) li salva nel path passato
/// </summary>
public interface IProductsSaver
{
    void Save(IDictionary<string, IList<string>> productsDict);
}