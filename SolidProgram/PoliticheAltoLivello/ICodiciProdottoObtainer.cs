﻿namespace SolidProgram.PoliticheAltoLivello;

/// <summary>
///     Politica ad alto livello: a partire da un path devo ottenere un dizinoari codice formato -> lista di codici prodotto
/// </summary>
public interface ICodiciProdottoObtainer
{
    IDictionary<string, IList<string>> GetProducts(string path);
}