﻿namespace SolidProgram.PoliticheAltoLivello;

/// <summary>
///     Politica ad alto livello: devo ottenere un path in qualche modo.
///     Un esempio potrebbe essere tramite form all'utente, oppure tramite file xml e così via
/// </summary>
public interface IPathObtainer
{
    string GetPath();
}