﻿using System.Text;
using SolidProgram.PoliticheAltoLivello;

namespace SolidProgram.PoliticheBassoLivello;

/// <summary>
///     Politica a basso livello: decido di salvare i prodotti su un csv
/// </summary>
public class SaveProductsToCsv : IProductsSaver
{
    public void Save(IDictionary<string, IList<string>> productsDict)
    {
        // Il path di esportazione sarà la directory corrente un un file con nome costante
        var exportedFile = Path.Join(Directory.GetCurrentDirectory(), "codici prodotto.csv");
        var csv = new StringBuilder();
        csv.AppendLine("Formato;Prodotto");
        foreach (var (codiceFormato, products) in productsDict)
        foreach (var product in products)
            csv.AppendLine($"{codiceFormato};{product}");
        // Salvo lo StringBuilder su un file
        File.WriteAllText(exportedFile, csv.ToString());
        Console.WriteLine($"Codici prodotto esportati correttamente in {exportedFile}");
    }
}