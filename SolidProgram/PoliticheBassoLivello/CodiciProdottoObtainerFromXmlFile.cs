﻿using System.Xml;
using SolidProgram.PoliticheAltoLivello;

namespace SolidProgram.PoliticheBassoLivello;

/// <summary>
/// Politica a basso livello: i codici prodotto li trovo parsando i file metadata.xml per ogni formato
/// </summary>
public class CodiciProdottoObtainerFromXmlFile : ICodiciProdottoObtainer
{
    private const string Metadatafile = "metadata.xml";
    private const string Codice = "codice";
    private const string Prodotto = "prodotto";

    public IDictionary<string, IList<string>> GetProducts(string path)
    {
        var output = new Dictionary<string, IList<string>>();
        try
        {
            if (Directory.Exists(path))
            {
                var formatiList = Directory.GetDirectories(path);
                foreach (var formato in formatiList)
                {
                    // Cerco il file metadata.xml
                    if (Directory.GetFiles(formato, Metadatafile).Length == 0)
                        continue;

                    // L'ho trovato, vado a cercare i tag che mi interessano:
                    var doc = new XmlDocument();
                    doc.Load(Path.Join(formato, Metadatafile));

                    var codiceFormato = doc.GetElementsByTagName(Codice)[0]?.InnerXml;
                    if (codiceFormato == null)
                        continue;
                    var productList = doc.GetElementsByTagName(Prodotto);
                    var products = new List<string>();
                    foreach (XmlNode product in productList)
                    {
                        var codiceProdotto = product?.InnerXml;
                        
                        if (codiceProdotto != null)
                            products.Add(codiceProdotto);
                    }
                    output.Add(codiceFormato, products);

                }

                return output;
                
            }
            else
            {
                throw new FileNotFoundException();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Errore durante l'esportazione dei codici prodotto");
            Console.WriteLine(e);
            throw;
        }
    }
}