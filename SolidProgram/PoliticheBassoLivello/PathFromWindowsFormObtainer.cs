﻿using SolidProgram.PoliticheAltoLivello;

namespace SolidProgram.PoliticheBassoLivello;

/// <summary>
///     Politica a basso livello: il path lo chiedo tramite modale all'utente
/// </summary>
public class PathFromWindowsFormObtainer : IPathObtainer
{
    public string GetPath()
    {
        return AskUserPath();
    }

    private static string AskUserPath()
    {
        var win = new FolderBrowserDialog
        {
            SelectedPath = Directory.GetCurrentDirectory(),
            Description = "Selezionare la directory in cui cercare i Formati:",
            ShowNewFolderButton = false
        };

        using (win)
        {
            return (win.ShowDialog() == DialogResult.OK ? win.SelectedPath : null) ?? string.Empty;
        }
    }
}