﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SolidProgram.PoliticheAltoLivello;
using SolidProgram.PoliticheBassoLivello;

namespace SolidProgram
{
    /// <summary>
    /// L'obiettivo di questo software è rispettare il più possibile i principi SOLID della buona programmazione.
    /// 
    /// S: Single Responsability: ogni classe ha una e una sola responsabilità. In questo esempio hanno anche un solo metodo.
    /// O: Open/Closed: Il codice è aperto alle estensioni (voglio salvare su database invece che su csv? Creo una nuova istanza di IProductsSaver e modifico solo riga 34 del main) ma chiuso alle modifiche (se voglio aggiungere funzionalità non modifico nulla delle classi esistenti)
    /// L: Liskov substitution principle: le classi figlie mantengono lo stesso comportamento delle classi padri. In questo caso le classi padri sono interfacce per cui il principio è garantito
    /// I: Interface segregation principle. Le interfacce devono essere molte, specifiche e piccole; ogni client deve dipendere dalle interfacce che usa, non da una singola interfaccia con tutto
    /// D: Dependency inversion principle. Una classe deve dipendere dalle politiche ad alto livello (astrazioni, interfacce) e non da quelle a basso livello. Il Program dipende infatti solo da interfacce astratte.
    ///
    /// Politiche di alto livello (non devono dipendere da quelle di basso livello):
    /// 1 - Ottenere un path dove trovo dei file xml (vengono da un path? da un database? sono xml o di altro formato?)
    /// 2 - Data una lista di file ottengo una lista di stringhe parsandoli
    /// 3 - Data una lista di stringhe le salvo (su database? su csv? su un server?)
    /// Politiche di basso livello (devono dipendere da quelle di alto livello):
    /// 1 - I file vengono da un path chiesto all'utente (file system)
    /// 2 - I file sono xml
    /// 3 - Ottengo le stringhe prodotto parsando xml
    /// 4 - Le salvo su un csv
    /// </summary>
    public class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            // Uso la DependencyInjection per definire le istanze delle interfacce che voglio.
            // Qualora volessi modificare un comportamento del mio software basta cambiare l'istanza da creare qui
            // Ovviamente questo è un esempio stupido per usare la DI, ma può avere senso a livello formativo per capire come funziona
            // In questo caso avrei potuto ottenere la stessa cosa con un AbstractFactory
            var host = DefineProgramDependency(args, out var provider);

            var formatsObtainer = provider.GetRequiredService<IPathObtainer>();
            var codiciProdottoObtainer = provider.GetRequiredService<ICodiciProdottoObtainer>();
            var productsSaver = provider.GetRequiredService<IProductsSaver>();

            var exporter = new ProductsExporter(formatsObtainer, codiciProdottoObtainer, productsSaver);
            exporter.Export();

            host.Run();
        }

        /// <summary>
        ///     Questo metodo definisce tutte le dipendenze del mio programma. Di fatto è l'unico punto dove tratterò classi concrete, tutto il resto sarà astratto
        /// </summary>
        private static IHost DefineProgramDependency(string[] args, out IServiceProvider provider)
        {
            using var host = Host.CreateDefaultBuilder(args)
                .ConfigureServices((_, services) =>
                    services.AddTransient<IPathObtainer, PathFromWindowsFormObtainer>()
                        .AddTransient<IProductsSaver, SaveProductsToCsv>()
                        .AddTransient<ICodiciProdottoObtainer, CodiciProdottoObtainerFromXmlFile>())
                .Build();
            using var serviceScope = host.Services.CreateScope();
            provider = serviceScope.ServiceProvider;
            return host;
        }
    }
}

