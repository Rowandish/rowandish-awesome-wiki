﻿using SolidProgram.PoliticheAltoLivello;

namespace SolidProgram;
/// <summary>
/// Classe che si occupa di esportare la lista dei prodotti. La classe rispetta tutti i principi SOLID quindi:
/// S: Single Responsability: ogni classe ha una e una sola responsabilità, che è la responsabilità principale del programma.
/// O: Open/Closed: Il codice è aperto alle estensioni ma chiuso alle modifiche, questa classe difficilmente cambierà dato che il suo comportamento si basa interamente su interfacce
/// L: Liskov substitution principle: In questo caso non è importante dato che non ho classi derivate di questa classe
/// I: Interface segregation principle. Le sono molte, specifiche e piccole e questa classe dipende esclusivamente dalle interfacce con i metodi che usa
/// D: Dependency inversion principle. La classe dipende solo dalle politiche ad alto livello (interfacce) e non da quelle a basso livello.
/// </summary>
public class ProductsExporter
{
    private readonly IPathObtainer _formatsObtainer;
    private readonly ICodiciProdottoObtainer _codiciProdottoObtainer;
    private readonly IProductsSaver _productsSaver;

    public ProductsExporter(IPathObtainer formatsObtainer, ICodiciProdottoObtainer codiciProdottoObtainer, IProductsSaver productsSaver)
    {
        _formatsObtainer = formatsObtainer;
        _codiciProdottoObtainer = codiciProdottoObtainer;
        _productsSaver = productsSaver;
    }

    public void Export()
    {
        // Ottengo il path dove sono i formati
        var formatsPath = _formatsObtainer.GetPath();
        // A partire dai formati ottengo un dizionario con i prodotti
        var products = _codiciProdottoObtainer.GetProducts(formatsPath);
        // Salvo il dizionario
        _productsSaver.Save(products);
    }
}