﻿namespace CleanArchitecture.DependencyInversion
{
    /// <summary>
    ///     Dipendendo dall'interfaccia <see cref="IDatabaseInteractor" />, che è presente nel componente delle politiche ad
    ///     alto livello, ho invertito le dipendenze. La mia classe è così chiusa alle modifiche e aperta alle estensioni.
    /// </summary>
    internal class DependencyInversionGood
    {
        private readonly IDatabaseInteractor _databaseManager;

        public DependencyInversionGood(IDatabaseInteractor databaseManager)
        {
            _databaseManager = databaseManager;
        }

        public void Method()
        {
            _databaseManager.Query("...");
        }
    }

    /// <summary>
    ///     Questa interfaccia permette l'inversione delle dipendenze. L'interfaccia appartiene alle politiche di alto livello
    ///     e la mia classe <see cref="DependencyInversionGood" /> dipende solo da questa ultima, senza sapere che ne esiste
    ///     una sua implementazione che fa cose.
    /// </summary>
    internal interface IDatabaseInteractor
    {
        void Query(string s);
    }

    /// <summary>
    ///     Questo componente ora dipende da un componente ad alto livello (<see cref="IDatabaseInteractor" />) potenzialmente
    ///     in una altra dll. Le modifiche a tale componente ad alto livello si rifletternanno su di me ma non il contrario.
    /// </summary>
    internal class DatabaseManagerGood : IDatabaseInteractor
    {
        public void Query(string s)
        {
        }
    }
}