﻿namespace CleanArchitecture.DependencyInversion
{
    /// <summary>
    ///     Assumiamo di avere due classi, una che si occupa della politica ad alto livello dell'applicazione (per esempio
    ///     delle logiche per la generazione di report di un database) e una che si occupa di politiche a basso livello (per
    ///     esempio il database). L'obiettivo è far sì che le politiche ad alto livello non dipendano da politiche a basso
    ///     livello, quindi che le modifiche a queste ultime non vadano ad inficiare i componenti ad alto livello.
    ///     
    /// </summary>
    internal class FinancialReportGenerationBad
    {
        private readonly DatabaseManagerBad _databaseManager;

        public FinancialReportGenerationBad(DatabaseManagerBad databaseManager)
        {
            _databaseManager = databaseManager;
        }

        public void Method()
        {
            _databaseManager.Query("...");
        }
    }
    /// <summary>
    /// DatabaseManagerBad è un componente a basso livello, presente potenzialmente anche in una dll separata. Se questo viene modificato sono obbligato a modificare anche <see cref="FinancialReportGenerationBad"/>, cosa indesiderabile.
    /// </summary>
    internal class DatabaseManagerBad
    {
        public void Query(string s)
        {
        }
    }
}