﻿using System;

namespace CleanArchitecture.LiskovSubstitutionPrinciple
{
    /// <summary>
    ///     Il principio definisce che gli oggetti della superclasse devono essere sostituibili con oggetti delle sue classi
    ///     figlie senza rompere l'applicazione. Questo significa che gli oggetti della sottoclasse devono comportarsi allo
    ///     stesso modo della superclasse.
    ///     Questo principio limita l'utilizzo dell'ereditarietà a cose che si comportanto in modo uguale, non che hanno le
    ///     stesse proprietà.
    ///     L'esempio classico è il quadrato figlio di rettangolo: nel mondo reale il quadrato è una forma di rettangolo,
    ///     quindi sembra corretto fare sì che questo erediti da questo ultimo, ma il comportamento è diverso: il quadrato ha i
    ///     lati uguali, il rettangolo no.
    ///     Il principio di LPS indica che se se le specifiche della superclasse (Rettangolo) indicano che altezza e larghezza devono essere modificate in modo indipendente, allora un quadrato non potrà mai essere figlio di rettangolo.
    ///     Se invece le specifiche del mio progetto dicono che il rettangolo è immutabile nelle dimensioni, allora il quadrato può esserne un sottotipo.
    ///     E' tutta una questione delle classi figlie che mantengono lo stesso comportamento delle classi padri.
    /// </summary>
    internal class LiskovSubstitutionPrincipleGood
    {
    }

    /// <summary>
    /// Rettangolo con larghezza e altezza entrambi con get e set;
    /// </summary>
    internal class Rectangle
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }

    /// <summary>
    ///     Il quadrato è un rettangolo con un vincolo: Width e Height devono essere uguali. Dato che questo vincolo non c'è
    ///     nella superclasse sto violando il LSP, ho effettuato una ereditarietà su un oggetto quando il suo comportamento è
    ///     diverso. Questo porta a problemi (devo aggiungere degli if sul tipo di dato) di struttura del codice.
    ///     Se avessi nel parent solo metodi get allora il principio sarebbe rispettato.
    /// </summary>
    internal class Square : Rectangle
    {
    }
}
