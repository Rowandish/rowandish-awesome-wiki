﻿namespace CleanArchitecture.InterfaceSegregationPrinciple
{
    /// <summary>
    ///     Assumiamo di avere la seguente classe, questa ha 3 metodi Method1, Method2, Method3. Assumiamo inoltre di avere 3
    ///     classi Utente che utilizzano solo rispettivamente il Method 1,2 e 3. quello che ottengo che <see cref="User1Bad" />
    ///     dipenderà inavvertitamente da Method2 e 3 anche se non le richiama. questa dipendenza fa sì che una modifica al
    ///     codice sorgente di Method2 nella classe <see cref="InterfaceSegregationPrincipleBad" /> porti alla ricompilazione e
    ///     deploy anche di <see cref="User1Bad" /> anche se lì nulla è cambiato.
    /// </summary>
    internal class InterfaceSegregationPrincipleBad
    {
        public void Method1()
        {
        }

        public void Method2()
        {
        }

        public void Method3()
        {
        }
    }

    internal class User1Bad
    {
        private readonly InterfaceSegregationPrincipleBad _interfaceSegregationPrincipleBad =
            new InterfaceSegregationPrincipleBad();

        public void Method()
        {
            _interfaceSegregationPrincipleBad.Method1();
        }
    }

    internal class User2Bad
    {
        private readonly InterfaceSegregationPrincipleBad _interfaceSegregationPrincipleBad =
            new InterfaceSegregationPrincipleBad();

        public void Method()
        {
            _interfaceSegregationPrincipleBad.Method2();
        }
    }

    internal class User3Bad
    {
        private readonly InterfaceSegregationPrincipleBad _interfaceSegregationPrincipleBad =
            new InterfaceSegregationPrincipleBad();

        public void Method()
        {
            _interfaceSegregationPrincipleBad.Method3();
        }
    }
}