﻿namespace CleanArchitecture.InterfaceSegregationPrinciple
{
    public interface IUser1Ops
    {
        void Method1();
    }

    public interface IUser2Ops
    {
        void Method2();
    }

    public interface IUser3Ops
    {
        void Method3();
    }

    /// <summary>
    ///     Ho applicato  il principio di segregazione a interfacce: i metodi Method1,2 e 3 ora sono metodi di 3 interfacce
    ///     distinte da cui eserita tutte la classe <see cref="InterfaceSegregationPrincipleGood" />.
    ///     La classe <see cref="User1Good" /> ora dipende solo da <see cref="IUser1Ops" /> che contiene solo
    ///     <see cref="IUser1Ops.Method1" /> e non gli altri 2 metodi.
    ///     In questo modo <see cref="User1Good" /> dipende solo da quello che gli serve e non da altre cose.
    ///     Questa buona pratica permette di evitare catene di dipendenze pericolose.
    ///     Vedi il libro Clean Architecture a pagina 68.
    /// </summary>
    internal class InterfaceSegregationPrincipleGood : IUser1Ops, IUser2Ops, IUser3Ops
    {
        public void Method1()
        {
        }

        public void Method2()
        {
        }

        public void Method3()
        {
        }
    }

    /// <summary>
    ///     La classe dipende solo da interfacce che contengono solo il metodo che mi serve, non tutto il resto
    /// </summary>
    public class User1Good
    {
        private readonly IUser1Ops _user1Operations;

        public User1Good(IUser1Ops user1Operations)
        {
            _user1Operations = user1Operations;
        }

        public void Method(IUser1Ops user1Operations)
        {
            _user1Operations.Method1();
        }
    }

    public class User2Good
    {
        private readonly IUser2Ops _user2Operations;

        public User2Good(IUser2Ops user2Operations)
        {
            _user2Operations = user2Operations;
        }

        public void Method()
        {
            _user2Operations.Method2();
        }
    }

    public class User3Good
    {
        private readonly IUser3Ops _user3Operations;

        public User3Good(IUser3Ops user3Operations)
        {
            _user3Operations = user3Operations;
        }

        public void Method()
        {
            _user3Operations.Method3();
        }
    }
}