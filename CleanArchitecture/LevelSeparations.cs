﻿using System;

namespace CleanArchitecture
{
    /// <summary>
    ///     Il "livello" in un software è la "distanza tra gli input e gli output", più lontana è una politica dagli input e
    ///     dagli ouput del sistema, maggiore è il suo livello.
    ///     Prendiamo un programma di crittografia che legge i caratteri da un device di input, li traduce usando una tabella e
    ///     poi li scrive tradotti su un device di output.
    ///     Il componente "Traslate" è di livello più elevato poichè il compoenente più lontano dagli input e dagli output.
    ///     In una buona architettura software le dipendenze del codice sorgente sono disaccoppiate rispetto al flusso di dati
    ///     ma accoppiate per livello.
    ///     Mostro qui il metodo veloce ma brutto e il metodo invece corretto.
    ///     Tutto questo è spiegato bene in "Clean Architecture" pagina 144 e seguenti.
    /// </summary>
    public class LevelSeparations
    {
        /// <summary>
        ///     Qeusta architettura è buona, il metodo "GoodArchitectureEncrypt" dipende solo da classi di alto livello
        ///     (ICharReader e ICharWriter) ed è indipendente dalle loro implementazioni.
        ///     Sarà il chiamante a istanziare il tipo corretto (magari con un AbstractFactory per evitare di dipendere anch'esso
        ///     da classi di basso livello).
        ///     Con un metodo implementato in questo modo, qualora nel futuro io modifichi dispositivo I/O non dovrò modificare
        ///     nulla di questa classe.
        /// </summary>
        public void GoodArchitectureEncrypt(ICharReader reader, ICharWriter writer)
        {
            writer.Write(Translate(reader.Read()));
        }

        /// <summary>
        ///     Questa architettura è errata: la funzione ad alto livello "BadArchitectureEncrypt" dipende funzioni a basso livello
        ///     ReadLine e WriteLine. Se un giorno non uso più la Console ma qualsiasi altra cosa devo riscrivere tutto.
        /// </summary>
        public void BadArchitectureEncrypt()
        {
            Console.WriteLine(Translate(Console.ReadLine()));
        }

        private string Translate(string line)
        {
            return $"--{line}--";
        }
    }

    public interface ICharWriter
    {
        void Write(string value);
    }

    public interface ICharReader
    {
        string Read();
    }
}